$$
\begin{aligned}
\text{Let } &f(x) = 2x + 1, \\
            &g(x) = x^2 - 2 \\
\end{aligned}
$$
$$
\begin{aligned}
A &= \int_a^b [f(x) - g(x)] \ dx \\
  &= \int_{-1}^3 2x+1 \ dx - \int_{-1}^3 x^2-2 \ dx \\
  &= \left[ x^2 + x \right]_{-1}^3 - \left[ \frac13 x^3 - 2x \right]_{-1}^3 \\
  &= 12 - \frac43 \\
  &= \frac{32}3
\end{aligned}
$$