# Classwork Section 3.6

### Together

A)

$$
\begin{aligned}
y &= \tanh(1+e^{2x}) \\
y' &= \sech^2(1+e^{2x}) \cdot e^{2x} \cdot 2 \\
y' &= 2e^{2x} \sech^2(1+e^{2x})
\end{aligned}
$$

B)

$$
\begin{aligned}
y &= x \sinh x - \cosh x \\
y' &= \sinh x + x \cosh x - \sinh x \\
y' &= x \cosh x
\end{aligned}
$$

C)

$$
\begin{aligned}
y &= \cosh(\ln x) \\
y' &= \frac{\sinh (\ln x)}{x}
\end{aligned}
$$

D)

$$
\begin{aligned}
y &= \ln(\cosh x) \\
y' &= \frac{\sinh x}{\cosh x} \\
y' &= \tanh x
\end{aligned}
$$

E)

$$
\begin{aligned}
y &= x \coth(1+x^2) \\
y' &= \coth(1+x^2) \cdot x \cdot -\csch^2 (1+x^2) \cdot 2x \\
y' &= -3x \coth(1+x^2) \csch^2(1+x^2)
\end{aligned}
$$

F)

$$
\begin{aligned}
y &= e^{\cosh x} \\
y' &= e^{\cosh x} \cdot \sinh x
\end{aligned}
$$

G)

$$
\begin{aligned}
y &= \csch x (1-\ln \csch x) \\
y' &= -\csch x \coth x \ (1-\ln \csch x) + \csch x \left(\frac{\csch x \coth x}{\csch x}\right) \\
y' &= -\csch x \coth x \ (1-\ln \csch x) + \csch x \coth x \\
y' &= \csch x \coth x \ (-(1 - \ln \csc x) + 1) \\
y' &= \csch x \coth x \ (\ln \csc x)
\end{aligned}
$$

H)

$$
\begin{aligned}
y &= \sech^2 {e^x} \\
y' &= 2 \left(\sech(e^x)\right) \cdot - \sech e^x \tanh e^x \cdot e^x \\
y' &= -2 e^x \left(\sech^2 (e^x)\right) \tanh e^x
\end{aligned}
$$

### Groups

A)

$$
\begin{aligned}
y &= \sinh^{-1} (\tan x) \\
y' &= \frac1{\sqrt{1+(\tan x)^2}} \cdot (\sec^2 x) \\
y' &= \frac{\sec^2 x}{\sqrt{1+(\tan x)^2}} \\
y' &= \frac{\sec^2 x}{\sqrt{\sec^2x}} \\
y' &= \frac{\sec^2 x}{\sec x} \\
y' &= \sec x
\end{aligned}
$$

B)

$$
\begin{aligned}
y &= \cosh^{-1} \sqrt{x} \\
y' &= \frac1{\sqrt{x-1}} \cdot \frac12 x ^{-\frac12} \\
y' &= \frac1{\sqrt{x-1}} \cdot \frac1{2\sqrt x} \\
\end{aligned}
$$

C)

$$
\begin{aligned}
y &= x \tanh^{-1} x + \ln \sqrt{1-x^2} \\
y' &= \tanh^{-1} x + \frac{x}{\sqrt{1-x^2}} + \frac{-2x}{2\sqrt{1-x^2}} \\
y' &= \tanh^{-1} x + \frac{x}{\sqrt{1-x^2}} - \frac{x}{\sqrt{1-x^2}} \\
y' &= \tanh^{-1} x
\end{aligned}
$$

D)

$$
\begin{aligned}
y &= \sech^{-1} e^{-x} \\
y' &= -\frac{-e^{-x}}{e^{-x} \sqrt{1-{e^{-x}}^2}} \\
y' &= \frac1{\sqrt{1-e^{-2x}}}
\end{aligned}
$$

E)

$$
\begin{aligned}
y &= \cosh^{-1} (\sec x) \\
y' &= \frac1{\sqrt{\sec^2 x - 1}} \cdot \sec x \tan x \\ 
y' &= \frac{\sec x \tan x}{\sqrt{\sec^2 x - 1}} \\
y' &= \frac{\sec x \tan x}{\sqrt{\tan^2 x}} \\
y' &= \sec x
\end{aligned}
$$

F)

$$
\begin{aligned}
y &= \log_5 \tanh \left(\cosh^{-1} (3x)\right) \\
y' &= \frac{\sech^2 \left(\cosh^{-1} (3x)\right) \cdot \frac3{\sqrt{(3x)^2-1}}}{\ln 5 \cdot \tanh \left(\cosh^{-1} (3x)\right)} \\
y' &= \frac{3 \sech^2 \left(\cosh^{-1} (3x)\right)}{\ln 5 \cdot \tanh \left(\cosh^{-1} (3x)\right) \cdot \sqrt{(3x)^2 -1}} \\
\end{aligned}
$$