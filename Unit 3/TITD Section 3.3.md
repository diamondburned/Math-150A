# TITD Section 3.3

## Logs & Exponents

### Exponential function family

$$
\begin{aligned}
e^{x+y} &= e^xe^y \\
e^{-x} &= \frac{1}{e^x} \\
e^{x-y} &= \frac{e^x}{e^y} \\
(e^x)^p &= e^{xp} \\
\ln(e^x) &= x \\
e^{\ln x} &= x \\
b^x &= e^{\ln b^x} = e^{x \ln b}
\end{aligned}
$$

### Logarithmic function family

$$
\begin{aligned}
\log(xy) &= \log x + \log y \\
\log(\frac xy) &= \log x - \log y \\
\log(x^p) &= p \log x \\
\log_b(b^x) &= x \\
\text{If} \ \log_b(y) = x \ \text{then} \ &= b^x = y \\
\log_ba &= \frac{\ln a}{\ln b}
\end{aligned}
$$

### Definitions

#### Derivatives of natural log functions

$$
\begin{aligned}
&\frac{d}{dx}(\ln x) = \frac1x \\
&\frac{d}{dx}(\ln |x|) = \frac1x \\
&\frac{d}{dx}(\ln |u(x)|) = \frac1{u(x)} \cdot \frac{d}{dx}(u(x))
\end{aligned}
$$

#### Derivatives of exponential functions

$$
\begin{aligned}
&\frac{d}{dx}(b^x) = xb^{x-1} \cdot \frac{d}{dx}b \\
&\frac{d}{dx}(e^{g(x)}) = e^{g(x)} \cdot \frac{d}{dx}g(x)
&\end{aligned}
$$

#### Derivatives of log base "b" functions

$$
\begin{aligned}
&\frac{d}{dx}(log_bx) = \frac1{x \ln b} \\
&\frac{d}{dx}(log_b|x|) = \frac1{x \ln b} \\
&\frac{d}{dx}(log_b|u(x)|) = \frac1{u(x) \ln b} \cdot \frac{d}{dx}u(x)
\end{aligned}
$$

### Examples

1. 
   
$$
\begin{aligned}
f(x) &= \ln(x^3) = 3 \ln x \\
f'(x) &= 3 \cdot \frac1x = \frac3x
\\[1em]
f(x) &= \ln(x^3) \\
f'(x) &= \frac1{x^3} \cdot 3x^2 = \frac{3x^2}{x^3} = \frac3x
\end{aligned}
$$

2. 

$$
\begin{aligned}
y &= t \cdot (\ln t)^2 \\
y' &= \frac{d}{dx}t \cdot (\ln t)^2 + t \cdot \frac{d}{dx}(\ln t)^2 \\
&\text{(Product rule is used)} \\
y' &= (\ln t)^2 + 2t \ln t \\
&\text{(Chain rule rule is used)} \\
\end{aligned}
$$