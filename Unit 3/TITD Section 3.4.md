# TITD Section 3.4

### Conceptual Understanding

1. *Half-life* is associated with the exponential decay model. The half-life is the time required for the quantity to be reduced in half.

### Practice

2. The half-life of radium-266 ($_{\ \ 88}^{266}Ra$) is 1590 years.

a.

$$
\begin{aligned}
50 &= 100e^{1590k} \\
0.5 &= e^{1590k} \\
\ln 0.5 &= \ln(e^{1590k}) \\
\ln 0.5 &= 1590k \ln(e) = 1590k \\
k &= \frac{\ln 0.5}{1590} \\[1em]
P(t) &= 100 e^{(\ln 0.5 / 1590)t}
\end{aligned}
$$

b.

$$
\begin{aligned}
P(1000) &= 100e^{(\ln 0.5 / 1590)(1000)} \\
&\approx 65 \ \text{mg}
\end{aligned}
$$

c.

$$
\begin{aligned}
P(t) &= 30 \ \text{mg} \\
30 &= 100 e^{(\ln 0.5 / 1590)t} \\
0.3 &= e^{(\ln 0.5 / 1590)t} \\
\ln 0.3 &= \left(\frac{t \ln 0.5}{1590}\right) \ln e \\
t &= 1590 \frac{\ln 0.3}{\ln 0.5} \\
t &\approx 2762 \ \text{years}
\end{aligned}
$$

3. Let $T(t)$ be the temperature of the object at time $t$, $T_s$ be the temperature of the surroundings, and $k$ be a constant, then

$$
\begin{aligned}
\frac{dT}{dt} = k(T - T_s)
\end{aligned}
$$

If a bottle of soda pop at room temperature ($72\oF$) is placed into a refrigerator where the temperature is $44\oF$, and after half an hour, the soda pop has cooled to $61\oF$, then

a.

$$
\begin{aligned}
61 &= 72e^{0.5k} \\
\ln \frac{61}{72} &= 0.5k \\
k &= \frac{\ln \frac{61}{72}}{0.5} \\
T(t) &= 72e^{\frac{\ln \frac{61}{72}}{0.5}t} \\[1em]
T(1) &= 72e^{\frac{\ln \frac{61}{72}}{0.5}} \approx 52 \oF
\end{aligned}
$$

b.

$$
\begin{aligned}
T(t) = 50 &= 72e^{\frac{\ln \frac{61}{72}}{0.5}t} \\
\ln \frac{50}{72} &= \frac{\ln \frac{61}{72}}{0.5} t \\[1em]
t &\approx 1.10 \ \text{h}
\end{aligned}
$$