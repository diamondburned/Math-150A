# Classwork Section 3.5

### Together

A)

$$
\begin{aligned}
\sin'^{-1} (5x) &= \frac1{\sqrt{1-(5x)^2}} \cdot 5 \\
&= \frac5{\sqrt{1-25x^2}}
\end{aligned}
$$

B)

$$
\begin{aligned}
y' &= \frac1{1+\left(\sqrt{2x+2}\right)^2} \cdot \frac2{2\sqrt{2x+1}} \\
&= \frac1{(2x+2) \sqrt{2x+1}}
\end{aligned}
$$

C)

$$
\begin{aligned}
y &= \cos^{-1} (x^2) \\
y' &= \frac{-1}{\sqrt{1-(x^2)^2}} \cdot 2x
&= -\frac{2x}{\sqrt{1-x^4}}
\end{aligned}
$$

D)

$$
\begin{aligned}
y' &= \frac1{\sqrt{1-(x^2-1)^2}} \cdot 2x \\
y' &= \frac{2x}{\sqrt{1-(x^2-1)^2}}
\end{aligned}
$$

E)

$$
\begin{aligned}
y' &= \sqrt1{1+(\cos x)^2} \cdot (-\sin x) \\
\end{aligned}
$$

F)

$$
\begin{aligned}
y' &= \frac{e^x}{\sqrt{1-e^{2x}}}
\end{aligned}
$$

G)

$$
\begin{aligned}
y &= \sqrt{x^2 - 1} \cdot \sec^{-1} x \\
y' &= \frac{2x}{2\sqrt{x^2-1}} \cdot \sec^{-1} x + \sqrt{x^2 - 1} \cdot \frac1{x\sqrt{x^2 - 1}} \\
y' &= \frac{x \sec^{-1} x}{\sqrt{x^2-1}} + \frac1x \\
\end{aligned}
$$

H)

$$
\begin{aligned}
y &= \sin^{-1} \sqrt{\sin x} \\
y' &= \frac1{\sqrt{1-\left(\sqrt{\sin x}\right)^2}} \cdot \frac{\cos x}{2 \sqrt{\sin x}} \\
y' &= \frac1{\sqrt{1-\sin x}} \cdot \frac{\cos x}{2 \sqrt{\sin x}}
\end{aligned}
$$

### Together

$$
\begin{aligned}
y &= \arctan{\frac{x}{2}} \text{ at the point} \left(2, \frac{\pi}4 \right) \\
y' &= \frac1{1 + (\frac{x}2)^2} \cdot \frac12 \\
&= \frac1{1+\frac{x^2}{4}} \cdot \frac12 \\
y'(2) &= \frac14 \\[2em]
y - \frac{\pi}4 &= m(x-2) \\
y - \frac{\pi}4 &= \frac14(x-2) \\
y &= \frac{x}4 - \frac{2 - \pi}{4}
\end{aligned}
$$

### Groups

A)

$$
\begin{aligned}
y &= (\arcsin 2x)^2 \\
y' &= 2(\sin^{-1} 2x) \cdot \frac1{\sqrt{1-(2x)^2}} \cdot 2 \\
y' &= \frac{4 \sin^{-1} 2x}{\sqrt{1-4x^2}}
\end{aligned}
$$

B)

$$
\begin{aligned}
y &= \arctan(\arcsin \sqrt{x}) \\
y' &= \frac1{1+\left(\sin^{-1} \sqrt{x}\right)^2} \cdot \frac1{\sqrt{1-(\sqrt{x})^2}} \cdot \frac12 x^{-\frac12} \\
y' &= \frac1{1+\left(\sin^{-1} \sqrt{x}\right)^2} \cdot \frac1{\sqrt{1-x}} \cdot \frac1{2 \sqrt x}
\end{aligned}
$$

C)

$$
\begin{aligned}
y &= x \tan^{-1} (4x) \\
y' &= \tan^{-1} (4x) + x (\frac1{1 + (4x)^2} \cdot 4) \\
y' &= \tan^{-1} (4x) + \frac{4x}{1 + 16x^2}
\end{aligned}
$$

D)

$$
\begin{aligned}
y &= x\ln(\arctan 5x) \\
y' &= \ln(\tan^{-1} 5x) + x \cdot \frac1{\tan^{-1} (5x)} \cdot \frac5{1+(5x)^2} \\
y' &= \ln(\tan^{-1} 5x) + \frac{5x}{\tan^{-1} (5x) (1+25x^2)}
\end{aligned}
$$