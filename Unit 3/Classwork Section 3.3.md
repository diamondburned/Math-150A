# Classwork Section 3.3

![](2020-09-23-08-45-23.png)

### Together

a)

$$
\begin{aligned}
& \log_5(a^5b^3) \\
&= \log_5a^5 + \log_3b^3 \\
&= 5 \log_5 a + 3 \log_5 b
\end{aligned}
$$

b)

$$
\begin{aligned}
& \log_3(z^2 \sqrt{x}) \\
&= \log_3 z^2 + \log_3 \sqrt{x} \\
&= 2 \log_3 z + \frac{1}{2} \log_3 x
\end{aligned}
$$

c)

$$
\begin{aligned}
& \log_4 (\frac{x^3}{y})^6 \\
&= \log_4 (\frac{x^{18}}{y^6}) \\
&= 18 \log_4 x - 6 \log_4 y
\end{aligned}
$$

d)

$$
\begin{aligned}
& \ln(xyz^3) \\
&= \ln x + \ln y + 3 \ln z
\end{aligned}
$$

#### Condense each expression to a single logarithm

a)

$$
\begin{aligned}
& 3 \log_9 x + 3 \log_9 y - 6 \log_9 z \\
&= \log_9 (x^3y^3) - 6 \log_9z \\
&= \log_9 \frac{x^3y^3}{z^6}
\end{aligned}
$$

b)

$$
\begin{aligned}
& 4 \log_5x + 3 \log_5 y - 6 \log_5 (2x + 5) \\
&= \log_5 \frac{x^4y^3}{(2x+5)^6}
\end{aligned}
$$

### Groups Review 1

a)

$$
\begin{aligned}
f(x) &= x^8 \\
f'(x) &= 8x^7
\end{aligned}
$$

b)

$$
\begin{aligned}
f(x) &= \sqrt[3]{3x+12} = (3x+12)^{\frac13} \\
f'(x) &= (3x+12)^{-\frac23}
\end{aligned}
$$

c)

$$
\begin{aligned}
f(x) &= 6x^{-9} \\
f'(x) &= -54x^{-10}
\end{aligned}
$$

d)

$$
\begin{aligned}
f(x) &= x^2 - 10x + 100 \\
f'(x) &= 2x - 10
\end{aligned}
$$

e)

$$
\begin{aligned}
f(x) &= (2x)^3 \\
f'(x) &= 6(2x)^2 
\end{aligned}
$$

f)

$$
\begin{aligned}
f(x) &= x^2 + \frac1{x^2} \\
f'(x) &= 2x - \frac{2x}{x^4}
\end{aligned}
$$

### Groups Review 2

a)

$$
\begin{aligned}
y &= (x^2 + 4x + 6)^5 \\
y' &= 5(x^2 + 4x + 6)^4 \cdot (2x + 4)
\end{aligned}
$$

b)

$$
\begin{aligned}
f(x) &= (x^3 - 5x)^4 \\
f'(x) &= 4(x^3 - 5x)^3 \cdot (3x - 5)
\end{aligned}
$$

c)

$$
\begin{aligned}
y &= \cos(x^3) \\
y' &= -3x^2 \sin(x^3) \\
\end{aligned}
$$

d)

$$
\begin{aligned}
y &= cos^3x \\
y' &= -3cos^2x \sin x
\end{aligned}
$$

e)

$$
\begin{aligned}
f(x) &= x^2 \cos x + x \tan x \\
f'(x) &= 2x \cos x - x^2 \sin x + \tan x + x \sec^2 x
\end{aligned}
$$

### Together Review 3

f)

$$
\begin{aligned}
y &= \sqrt{x^2 - 7x} \\
y' &= \frac12 \sqrt{x^2 - 7x}^{-\frac12} \cdot (2x - 7) \\
y' &= \frac{2x-7}{\sqrt{x^2 - 7x}^{\frac12}}
\end{aligned}
$$

g)

$$
\begin{aligned}
y &= (x^2 - 2x - 5)^{-4} \\
y' &= -4 (x^2 - 2x - 5)^{-5} \cdot (2x-2) \\
y' &= \frac{-4(2x-2)}{(x^2 - 2x - 5)^{-5}}
\end{aligned}
$$

h)

$$
\begin{aligned}
f'(x) &= \frac32 (x-\frac1x)^{\frac12} \cdot (1 + \frac1{x^2})
\end{aligned}
$$

i)

$$
\begin{aligned}
y &= (\sin (\cos 4x))^ 2 \\
y' &= 2 (\sin (\cos 4x)) \cdot \cos(\cos 4x) \cdot (-\sin(4x)) \cdot 4 \\
y' &= -8 (\sin (\cos 4x)) \cos(\cos 4x) \sin(4x)
\end{aligned}
$$

j)

$$
\begin{aligned}
y &= \sin^3(2x+3) \\
y' &= 3(\sin 2x+3)^2 \cdot \cos(2x+3) \cdot 2 \\
y' &= 6(\sin 2x+3)^2 \cdot \cos(2x+3)
\end{aligned}
$$

### Together

a)

$$
\begin{aligned}
f(x) &= x^2e^x \\
f'(x) &= 2xe^x + x^2e^x
\end{aligned}
$$

b)

$$
\begin{aligned}
f(x) &= (\ln x)^{\frac12} \\
f'(x) &= \frac12(\ln x)^{-\frac12} \cdot \frac1x \\
f'(x) &= \frac1{2x \sqrt{\ln x}}
\end{aligned}
$$

c)

$$
\begin{aligned}
f'(x) = \frac{\cos x}{\ln_{10} (2+\sin x)}
\end{aligned}
$$

d)

$$
\begin{aligned}
f'(x) = \frac1x
\end{aligned}
$$

### Groups

e)

$$
\begin{aligned}
f'(x) = \frac{e^x + xe^x}{\ln 5 (xe^x)}
\end{aligned}
$$

f)

$$
\begin{aligned}
y &= 3^{\sin 3x} \\
y' &= (\ln 3) 3^{\sin 3x} \cdot 3 \cos 3x
\end{aligned}
$$

g)

$$
\begin{aligned}
y' &= \frac{-e^{-x}}{\ln 2 \cdot e^{-x}} \cos \pi x + \log_2 e^{-x} \cdot -\pi \sin \pi x \\
y' &= -\frac{1}{\ln 2} \cos \pi x + log_2 e^{-x} \pi \sin \pi x
\end{aligned}
$$

h)

$$
\begin{aligned}
f(x) &= \frac{e^x - e^{-x}}{e^x + e^{-x}} \\
f'(x) &= \frac{\frac{d}{dx}(e^x-e^{-x}) \cdot (e^x + e^{-x}) - (e^x - e^{-x})\cdot \frac{d}{dx}(e^x + e^{-x})}{(e^x + e^{-x})^2} \\
f'(x) &= \frac{(e^x - xe^{-x}) \cdot (e^x + e^{-x}) - (e^x - e ^{-x}) \cdot (e^x + xe^{-x})}{(e^x + e^{-x})^2}
\end{aligned}
$$

i)

$$
\begin{aligned}
y' = \frac{12x^2 + 1}{4x^3 + x}
\end{aligned}
$$

### Example 2

a)

$$
\begin{aligned}
y &= x^x \\
\ln y &= \ln x^x \\
\ln y &= x \ln x \\
\frac1y \cdot y' &= \ln x + \frac{x}{x} \ \small{\text{(take the derivative)}} \\
\frac1y \cdot y' &= \ln x + 1 \\
y' &= y(\ln x + 1) \ \small{\text{(substitute} \ y = x^x \text{)}} \\
y' &= x^x \ln x + x^x
\end{aligned}
$$

b)

$$
\begin{aligned}
\ln y &= \sqrt x \ln x \\
\frac1y \cdot y' &= \frac1{2\sqrt{x}} \ln x + \frac{\sqrt x}{x} \\
y' &= (\frac1{2\sqrt x} \ln x + \frac{\sqrt x}{x}) x^{\sqrt x}
\end{aligned}
$$

c) Skip.

d)

$$
\begin{aligned}
\ln y &= \ln\left(\frac{x \sqrt{2x+1}}{e^x \sin^3x}\right) \\
\ln y &= \ln x + \frac12 \ln (2x + 1) - (x \ln e + 3 \ln(\sin x)) \\
\frac1y \cdot y' &= \frac1x + \frac{2}{2(2x+1)} - 1 - \frac{3 \cos x}{\sin x} \\
\frac1y \cdot y' &= \frac1x + \frac1{2x+1} - 1 - 3 \cot x \\
y' &= \left[\frac1x + \frac1{2x+1} - 1 - 3 \cot x\right]\left[\frac{x \sqrt{2x+1}}{e^x \sin^3x}\right]
\end{aligned}
$$