# Classwork Section 3.7

## Type 1: Simple Polynomials and Powers

### Ex #1

$$
\begin{aligned}
&\lim_{x \rightarrow 5} \frac{2x^2 - 9x - 5}{x - 5} \\
&\lim_{x \rightarrow 5} \frac{50 - 45 - 5}{0} = \frac00 \\[1em]
&\lim_{x \rightarrow 5} \frac{4x-9}{1} \overset{\text{LH}}{=} 11 \\
&\lim_{x \rightarrow 5} \frac{2x^2 - 9x - 5}{x - 5} = 11
\end{aligned}
$$

### Ex #2

$$
\begin{aligned}
&\lim_{x \rightarrow -5} = \frac{\sqrt{4 - x} - 3}{x+5} \\
&\lim_{x \rightarrow -5} = \frac{\sqrt{4 - (-5)} - 3}{-5+5} = \lim_{x \rightarrow -5} \frac{\sqrt{9} - 3}{-5+5} = \frac00 \\[1em]
&\lim_{x \rightarrow -5} \overset{\text{LH}}{=} \frac{\frac1{2\sqrt{4-x}}\cdot (-1)}{1} = \frac{-1}{2(3)} = -\frac16
\end{aligned}
$$

## Type 2: Repeated Use of L'Hospital's Rule

### Ex #3

$$
\begin{aligned}
&\lim_{x \rightarrow - \infty} \frac{x^2}{e^{-x}} = \frac\infty\infty \\
&\lim_{x \rightarrow - \infty} \frac{x^2}{e^{-x}} \overset{\text{LH}}{=} \frac{2x}{e^{-x}} = \frac\infty\infty \\
&\lim_{x \rightarrow - \infty} \frac{2x}{e^{-x}}  \overset{\text{LH}}{=} \frac2{e^{-x}} = 0
\end{aligned}
$$

### Ex #4

$$
\begin{aligned}
&\lim_{x \rightarrow -2} = \frac00 \\
\text{LH} &\lim_{x \rightarrow -2} \frac{3x^2 + 2x - 8}{3x^2 + 16x + 20} = \frac00 \\
\text{LH} &\lim_{x \rightarrow -2} \frac{6x+2}{6x+16} = -\frac52 \\
&\lim_{x \rightarrow -2} \frac{x^3 + x^2 - 8x - 12}{x^3 + 8x^2 + 20x + 16} = -\frac52
\end{aligned}
$$

## Type 3: Trigonometry and Transcendental Functions

### Ex #5

$$
\begin{aligned}
&\lim_{x \rightarrow 0} \frac{\sin x}{x} \cdot \lim_{x \rightarrow 0} \frac{1 - \cos x}{x} = 0 \\
\text{LH} &\lim_{x \rightarrow 0} \frac{\cos x}{1} \cdot \lim_{x \rightarrow 0} \frac{\sin x}{1} = 1 \cdot 0 = 0 \\
&\lim_{x \rightarrow 0} \frac{\sin x (1 - \cos x)}{x^2} = 0
\end{aligned}
$$

### Ex #6

$$
\begin{aligned}
&\lim_{x \rightarrow 1} \frac{\ln 1 - 1 + 1}{e^1 - e} = \frac00 \\
\text{LH} &\lim_{x \rightarrow 1} \frac{\frac1x - 1}{e^x} = \frac{1-1}{e} = 0 \\
&\lim_{x \rightarrow 1} \frac{\ln x - x + 1}{e^x - e} = 0
\end{aligned}
$$

## Type 4: L'Hospital's Rule for $\pm\frac\infty\infty$

### Ex #7

$$
\begin{aligned}
&\lim_{x \rightarrow \infty} = \frac\infty\infty \\
\lh &\lim_{x \rightarrow \infty} \frac{9x^2 + 10x - 3}{12x^2} = \frac\infty\infty \\
\lh &\lim_{x \rightarrow \infty} \frac{18x + 10}{24x} = \frac\infty\infty \\
\lh &\lim_{x \rightarrow \infty} \frac{18}{24} = \frac34 \\
&\lim_{x \rightarrow \infty} \frac{3x^3 + 5x^2 - 3x - 2}{4x^3 - 6} = \frac34
\end{aligned}
$$

### Ex #8

$$
\begin{aligned}
&\lim_{x \rightarrow \infty} = \frac\infty\infty \\
\lh &\lim_{x \rightarrow \infty} = \frac{8x - 3}{2e^{2x} + \frac1x} = \frac\infty\infty \\
\lh &\lim_{x \rightarrow \infty} = \frac8{4e^{2x} - \frac1{x^2}} = 0 \\
&\lim_{x \rightarrow \infty} \frac{4x^2 - 3x + 2}{e^{2x} + \ln x} = 0
\end{aligned}
$$