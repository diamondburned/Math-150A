# TITD Section 3.7

### Conceptual

If we have functions $f(x)$ and $g(x)$ such that $\lim_{x \rightarrow a}{\frac{f(x)}{g(x)}} = \frac00$ OR $\lim_{x \rightarrow a}{\frac{f(x)}{g(x)}} = \frac{\infty}{\infty},$ then we say those limits have $\lim_{x \rightarrow a}{\frac{f'(x)}{g'(x)}}$ form.

### Examples

1. L'Hopital rule may be applied here because:

$$
\begin{aligned}
\lim_{x \rightarrow 0} \frac{\sin x}x
    =& \lim_{x \rightarrow 0} \frac{\sin 0}0 = \frac00
\\[1em]
\lim_{x \rightarrow 0} \frac{\sin x}x
    \overset{\text{LH}}{=}&
    \lim_{x \rightarrow 0} \cos x = 1
\end{aligned}
$$

2. L'Hopital rule may be applied twice here because

$$
\begin{aligned}
\lim_{x \rightarrow \infty} \frac{4x^2 - 5x}{1 - 3x^2}
    =& \lim_{x \rightarrow \infty} \frac{\infty - \infty}{1 - \infty}
\\[1em]
\lim_{x \rightarrow \infty} \frac{4x^2 - 5x}{1 - 3x^2}
    \overset{\text{LH}}{=}&
    \lim_{x \rightarrow \infty} \frac{8x - 5}{-6x}
    = \frac{\infty - 5}{- \infty}
\\[1em]
\lim_{x \rightarrow \infty} \frac{8x - 5}{-6x}
    \overset{\text{LH}}=&
    \lim_{x \rightarrow \infty} -\frac{4}{3}
\end{aligned}
$$