# TITD Section 3.6

### Definitions

$$
\begin{aligned}
\sinh x &= \frac{e^x-e^{-x}}{2} & \ddx (\sinh x) &= \cosh x \\
\cosh x &= \frac{e^x+e^{-x}}{2} & \ddx (\cosh x) &= \sinh x \\
\tanh x &= \frac{\sinh x}{\cosh x} & \ddx (\tanh x) &= \sech^2 x \\
\csch x &= \frac1{\sinh x} & \ddx (\csch x) &= -\csch x \coth x \\
\sech x &= \frac1{\cosh x} & \ddx (\sech x) &= -\sech x \tanh x \\
\coth x &= \frac{\cosh x}{\sinh x} & \ddx (\coth x) &= -\csch^2 x \\
\end{aligned}
$$

$$
\begin{aligned}
\ddx (\sinh^{-1} x) &= \frac1{\sqrt{1+x^2}} & \ddx (\csch^{-1} x) &= -\frac1{|x|\sqrt{x^2+1}} \\
\ddx (\cosh^{-1} x) &= \frac1{\sqrt{x^2-1}} & \ddx (\sech^{-1} x) &= -\frac1{x\sqrt{1-x^2}} \\
\ddx (\tanh^{-1} x) &= \frac1{1-x^2} & \ddx (\coth^{-1} x) &= \frac1{1-x^2} \\
\end{aligned}
$$

### Example

1. 

$$
\begin{aligned}
f(x) &= x \tanh(3x-2) \\
f'(x) &= \tanh(3x-2) \cdot 3x \sech^2(3x-2)
\end{aligned}
$$

2. 

$$
\begin{aligned}
f(x) &= \sinh^{-1} (x^2 - 1) \\
f'(x) &= \frac{2x}{\sqrt{1+(x^2-1)^2}}
\end{aligned}
$$

3. 

$$
\begin{aligned}
f(x) &= \ln \cosh(5x^2 - 2x) \\
f'(x) &= -\frac{\csch^2 (5x^2 - 2x) \cdot (10x-2)}{\cosh (5x^2 - 2x)}
\end{aligned}
$$