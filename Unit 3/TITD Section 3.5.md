# TITD Section 3.5

### Definitions

$$
\begin{aligned}
\sin^{-1}x &= \arcsin x = \frac1{\sin x} \\
\sin'^{-1} x &= \frac1{\sqrt{1-x^2}}
\\[1em]
\cos^{-1}x &= \arccos x = \frac1{\cos x} \\
\cos'^{-1} x &= -\frac1{\sqrt{1 - x^2}}
\\[1em]
\tan^{-1}x &= \arctan x = \frac1{\tan x} \\
\tan'^{-1}x &= \frac1{1 + x^2}
\\[1em]
\csc^{-1} x &= \text{arccsc } x = \frac1{\csc x} \\
\csc'^{-1} x &= -\frac1{|x|\sqrt{x^2-1}}
\\[1em]
\sec^{-1} x &= \text{arcsec } x = \frac1{\sec x} \\
\sec'^{-1} x &= \frac1{|x|\sqrt{x^2-1}}
\\[1em]
\cot^{-1}x &= \text{arccot } x \\
\cot'^{-1}x &= -\frac1{1 + x^2}
\end{aligned}
$$

### Example

1) 

$$
\begin{aligned}
f(x) &= \sin^{-1} (x^2 - 1) \\
f'(x) &= \frac1{\sqrt{1-(x^2-1)^2}} \cdot 2x \\
f'(x) &= \frac{2x}{\sqrt{1-(x^2-1)^2}}
\end{aligned}
$$

2) 

$$
\begin{aligned}
g(x) &= \cos (\tan^{-1} 3x) \\
g'(x) &= -\frac1{\sqrt{1-(\tan^{-1} 3x)^2}} \cdot \frac1{1+(3x)^2} \cdot 3 \\
g'(x) &= -\frac3{\left[\sqrt{1-(\tan^{-1} 3x)^2}\right]\big(1+(3x)^2\big)}
\end{aligned}
$$