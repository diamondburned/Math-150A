# Classwork Section 3.4

### Together

$$
\begin{aligned}
A(t) &= 1000e^{0.3t} \\
A'(t) &= 1000e^{0.3t} 0.3 \\
A'(t) &= 300e^{0.3t} \\
\frac{A'(t)}{A(t)} &= \frac{300e^{0.3t}}{1000^{0.3t}} = 0.3 \\[1em]
A'(4) &= 300e^{0.3 \cdot 4} = 300e^{1.2} = 996 \ \text{m/day}
\end{aligned}
$$

### Together

a.

$$
\begin{aligned}
P(t) &= 500000(1+0.05)^t
\end{aligned}
$$

b.

$$
\begin{aligned}
P'(t) &= 500000(1.05)^t \cdot \ln(1.05) \\
P'(t) &= 24395(1.05)^t
\end{aligned}
$$

c.

$$
\begin{aligned}
P'(10) &= 24395(1.05)^{10} \\
P'(10) &= 39737
\end{aligned}
$$

### Together

a.

$$
\begin{aligned}
m(t) &= a_0 e^{kt} \\
m(t) &= 9 e^{kt} \\[1em]
4.5 &= 9 e^{12k} \\
\ln \frac{4.5}{9} &= 12k \ln e \\
\ln 0.5 &= 12k \\
\frac{\ln 2^{-1}}{12} &= k \\[1em]
m(t) &= 9e^{\frac{\ln 2^{-1}}{12}t} \\
m(t) &= 9(2)^{-\frac{t}{12}}
\end{aligned}
$$

b.

$$
\begin{aligned}
m'(t) &= 9\left(2^{-\frac{t}{12}}\right)(\ln 2)\left(-\frac{1}{12}\right) \\
m'(t) &= -\frac34 \cdot \ln 2 \cdot \left(2^{-\frac{t}{12}}\right)
\end{aligned}
$$

c.

$$
\begin{aligned}
m'(4) &= -\frac34 \ln 2 \left(2^{-\frac4{12}}\right) = -0.4126 \ \text{g/h}
\end{aligned}
$$

### Together

a.

$$
\begin{aligned}
N(0) &= 5.3e^{0.093(0)^2 - 0.87(0)} \\
N(0) &= 5.3 \ \text{thousands of cases} \\[1em]
N(4) &= 5.3e^{0.093(4)^2 - 0.87(4)} \\
N(4) &= 0.723 \ \text{thousands of cases}
\end{aligned}
$$