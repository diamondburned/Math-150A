# Classwork Section 7.1

1.

![](2020-11-30-08-54-11.png)

$$
\begin{aligned}
y = x^2 \ ; y = 4x \\
x^2 &= 4 \\
x^2 - 4x &= 0 \\
x(x-4) &= 0 \\
x = 0  \ &; x = 4 \\
(0, 0) \ &; (4, 16)
\end{aligned}
$$

$$
\begin{aligned}
&\int_0^4 4x - x^2 \ dx \\
&= \frac{4x^2}2 - \frac{x^3}3 \Big]_0^4 \\
A &= 32 - \frac{64}3 \\
  &= \frac{32}3
\end{aligned}
$$

2.

![](2020-11-30-08-58-34.png)

$$
\begin{aligned}
x^2 + 1 &= 5 \\
x^2 - 4 &= 0 \\
(x-2)(x+2) &= 0 \\
x = 2 \ &, x = -2 \\
(2,5) \ &, (-2,5) \\
&\int_{-2}2 5 - (x^2 + 1) \ dx \\
&= 2 \int_0^2 5 - x^2 - 1 \ dx \\
&= 2 \int_0^2 4 - x^2 dx \\
&= 2 \left[4 - x^2\right] \\
&= 2 \left[4x - \frac{x^3}3\right]_0^2 \\
&= 2 \left[8 - \frac83\right] \\
&= 2 \left[\frac{16}{3} \right] \\
&= \frac{32}3
\end{aligned}
$$

3.

![](2020-11-30-09-02-16.png)

$$
\begin{aligned}
x - y &= 2 \\
x &= y + 2 \\
y^2 &= y + 2 \\
y^2 - y - 2 &= 0 \\
(y-2)(y+1) &= 0 \\
y = 2 \ &, y = 1 \\
(4,2) \ &, (1, -1) \\
&\int_{-1}^2 y + 2 - y^2 \ dy \\
&= \left[\frac{y^2}2 + 2y - \frac{y^3}3 \right]_{-1}^2 \\
&= \left[2 - 4 - \frac83\right] - \left[\frac12 - 2 - \frac13\right] \\
&= \frac92
\end{aligned}
$$

4.

![](2020-11-30-09-10-40.png)

$$
\begin{aligned}
y^2 &= 4 + x \\
x &= y^2 - 4 \\
y^2 + x &= 2 \\
x &= y^2 - 2 \\
y^2 - 4 &= 2 - y^2 \\
2y^2 &= 6 \\
y^2 &= 3 \\
y &= \plusmn \sqrt3 \\
(-1, -\sqrt3) \ &, (-1, \sqrt3) \\
A &= \int_{-\sqrt3}^{\sqrt3} 2 - y^2 - y^2 + 4 \ dy \\
  &= 2 \int_0^{\sqrt3} 2 - y^2 - y^2 + 4 \ dy \\
  &= 2 \int_0^{\sqrt3} 6 - 2y^2 \ dy \\
  &= 2 \left[4y - \frac{2y^3}3\right]_0^{\sqrt3} \\
  &= 2 \left[6 \sqrt3 - 2 \sqrt3\right] \\
  &= 8 \sqrt3
\end{aligned}
$$

5.

![](2020-11-30-09-21-58.png)

$$
\begin{aligned}
(x-1)^3 &= x - 1 \\
x^3 - 3x^2 + 3x - 1 &= x - 1 \\
x^3 - 3x^2 + 2x &= 0 \\
x (x^2 - 3x + 2) &= 0 \\
x (x-2)(x-1) &= 0 \\
x &= 0, (0, -1) \\
x &= 2, (2, 1) \\
x &= 1, (1, 0) \\
\end{aligned}
$$

$$
\begin{aligned}
A &= \int_0^1 (x-1)^3 - (x-1) \ dx + \int_1^2 (x-1) - (x-1)^3 \ dx \\
  &= \int_0^1 x^3 - 3x^2 + 2x \ dx + \int_1^2 - x^3 + 3x^2 - 2x \ dx \\
  &= \Big[ \frac14x^4 - x^3 + x^2 \Big]_0^1 + \Big[ -\frac14x^4 + x^3 - x^2 \Big]_0^1 \\
  &= \frac12
\end{aligned}
$$

6.

![](2020-11-30-09-30-14.png)

$$
\begin{aligned}
2x + 5 &= x^2 + 2x + 1 \\
0 &= x^2 - 4 \\
x &= \plusmn 2 \\
(-2, 1) \ &, (2, 9) \\
A &= \int_{-2}^2 (2x+5) - \left[x^2 + 2x + 1\right] \ dx \\
  &= \int_{-2}^2 -x^2 + 4 \ dx \\
  &= -\frac13 x^3 + 4x \Big]_{-2}^2 \\
  &= \frac{32}3
\end{aligned}
$$

7.

![](2020-11-30-09-30-31.png)

$$
\begin{aligned}
x^2 - 4x + 3 &= -x^2 + 2x + 3 \\
2x^2 - 6x &= 0 \\
2x(x-3) &= 0 \\
x = 3  \ &, x = 0 \\
(0, 3) \ &, (3, 0) \\
\end{aligned}
$$

$$
\begin{aligned}
A &= \int_0^3 -x^2 + 2x + 3 - x^2 + 4x - 3 \ dx \\
  &= \int_0^3 -2x^2 + 6x \ dx \\
  &= -\frac23x^3 + 3x^2 \Big]_0^3 \\
  &= 9
\end{aligned}
$$

8.

![](2020-11-30-09-34-23.png)

$$
\begin{aligned}
\frac12 x^2 &= \frac1{1+x^2} \\
x^2 + x^4 &= 2 \\
x^4 + x^2 - 2 &= 0 \\
(x^2 + 2)(x^2 - 1) &= 0 \\
x^2 - 1 &= 0 \\
x &= \plusmn 1 \\
(1, \frac12) \ &, (-1, \frac12)
\end{aligned}
$$

$$
\begin{aligned}
A &= \int_{-1}^1 \frac1{1+x^2} - \frac1{2x^2} \ dx \\
&\text{Do not integrate.}
\end{aligned}
$$

9.

$$
\begin{aligned}
2 \sin x &= \tan x \\
(\frac{-\pi}3, 1) \ &, (0, 0) \ , (\frac\pi3, 1) \\
A &= \int_{-\frac\pi3}^0 \tan x - 2\sin x \ dx + \int_0^{\frac\pi3} 2\sin x - \tan x \ dx \\
&\text{Do not integrate.}
\end{aligned}
$$

10. Skip

11.

![](2020-12-01-20-36-12.png)

$$
\begin{aligned}
x^2 &= 9 \\
x &= 2, x = -2 \\[1em]
\frac1{x^2} &= 9 \\
x^2 &= \frac19 \\
x &= \frac13, x = -\frac13 \\[1em]
x^2 &= \frac1{x^2} \\
x^2 - \frac1{x^2} &= 0 \\
x^4 - 1 &= 0 \\
x^4 &= 1 \\
x &= 1, x = -1 \\

\end{aligned}
$$

12.

![](2020-11-30-09-44-10.png)

$$
\begin{aligned}
-x &= 4x \\
x &= 0 \\[1em]
12 - x^2 &= 4x \\
x^2 + 4x - 12 &= 0 \\
(x+6)(x-2) &= 0 \\
x = -6 \ &, x = 2 \\[1em]
12 - x^2 &= -x \\
x^2 - x - 12 &= 0 \\
(x-4)(x+3) &= 0 \\
x = 4 \ &, x = -3
\end{aligned}
$$

$$
\begin{aligned}
A &= \int_{-3}^0 \ dx + \int_0^2 \ dx \\
  &= \int_{-3}^0 12 - x^2 - (-x) \ dx + \int_0^2 12 - x^2 - (4x) \ dx \\
  &= \int_{-3}^0 12 + x - x^2 \ dx + \int_{-3}^0 12 + 4x - x^2 \ dx \\
  &= 12x + \frac12x^2 - \frac13x^3 + 12x + 2x^2 - \frac13x^3 \Big]_{-3}^0 \\
  &= 24x + \frac32x^2 - \frac23x^3 \Big]_{-3}^0 \\
  &= -\frac{81}2
\end{aligned}
$$