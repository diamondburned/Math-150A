# TITD Section 7.1

1.

$$
\begin{aligned}
\text{Let } &f(x) = 2x + 1, \\
            &g(x) = x^2 - 2 \\
\end{aligned}
$$

$$
\begin{aligned}
A &= \int_a^b [f(x) - g(x)] \ dx \\
  &= \int_{-1}^3 2x+1 \ dx - \int_{-1}^3 x^2-2 \ dx \\
  &= \left[ x^2 + x \right]_{-1}^3 - \left[ \frac13 x^3 - 2x \right]_{-1}^3 \\
  &= 12 - \frac43 \\
  &= \frac{32}3
\end{aligned}
$$

2.

![](2020-11-29-18-11-38.png)

$$
\begin{aligned}
\text{Let } &f(y) = 2y^2, \\
            &g(y) = 4 + y^2 \\
\end{aligned}
$$

$$
\begin{aligned}
A &= \int_a^b [f(y) - g(y)] \ dy \\
  &= \int_{-2}^2 2y^2 - 4 - y^2 \ dy \\
  &= \int_{-2}^2 y^2 - 4 \ dy \\
  &= \left[ \frac13 y^3 \right]_{-2}^2 - 16 \\
  &= \frac{16}3 - 16 \\
  &= -\frac{32}3 \\
\end{aligned}
$$