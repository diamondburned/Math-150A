# TITD Section 4.4

### Conceptual & Exercise

1. $f(x)$ is a polynomial function.
2. The behavior as $x$ approaches $-\infty$ and $\infty$ is $\infty$.
3. The domain of the function is $(-\infty, \infty)$.
4. x-intercepts: $x=0$, $x=4$; y-intercepts: $y=0$.
5. Local behaviors:

$$
\begin{aligned}
f(x) &= x^4 - 4x^3 \\
f'(x) &= 4x^3 - 12x^2 \\
0 &= 4x^3 - 12x^2 \\
0 &= 4x^2(x - 3) \\
x &= 0 \ \ \ \ \ \ \ \ \ \ \ \ x = 3
\end{aligned}
$$

Critical values at $(0, 0)$ and $(3, -27)$.

| -1  | 0   | 2   | 3   | 5   |
| --- | --- | --- | --- | --- |
| ↘   | →   | ↘   | →   | ↗   |

The function is increasing over the interval $(3, \infty)$ and decreasing over the interval $(-\infty, 0) \cup (0, 3)$.

$$
\begin{aligned}
f'(x) &= 4x^3 - 12x^2 \\
f''(x) &= 12x^2 - 24x \\
0 &= 12x^2 - 24x \\
0 &= 12x(x - 2) \\
x &= 0 \ \ \ \ \ \ \ \ \ \ \ \ \ x = 2
\end{aligned}
$$

Inflection point at $(0, 0)$ and $(2, 0)$.

| -1        | 0   | 1         | 2   | 3         |
| --------- | --- | --------- | --- | --------- |
| $\bigcup$ | -   | $\bigcap$ | -   | $\bigcup$ |

The function is concave up over the interval $(-\infty, 0)$ and $(2, \infty)$ and concave down over the interval $(0, 2)$.

![](2020-10-17-01-59-06.png)