# Classwork Section 4.2

### Together

![](2020-10-15-18-17-59.png)

3.

$f(x)$ has $x = c$ at $x \approx 2.5$ and $x \approx 7$.

$g(x)$ has $x = c$ at $x = 5$.

$h(x)$ has $x = c$ at $x \approx 6.25$.

$k(x)$ has $x = c$ at $x \approx 3.5$.

4.

$f(x)$, $g(x)$ and $k(x)$ are continuous on the given intervals.

5.

$f(x)$ is differentiable on the given interval.

$g(x)$ and $k(x)$ are not differentiable because there's a pointy change within the interval.

6.

You would be able to satisfy theconditions for MVT because $[1,5]$ is both continuous and differentiable.

### Together

a.

![](2020-10-14-09-38-57.png)

$$
\begin{aligned}
\text{Slope AB} &= \frac{y_2 - y_1}{x_2 - x_1} \\[1em]
&= \frac{2+6}{0+2} \\
&= 4 \\[1em]
f'(x) &= 4x \\
-4c &= 4 \\
c &= -1 \\
\end{aligned}
$$

b.

![](2020-10-16-15-47-13.png)

$$
\begin{aligned}
\text{Slope} &= \frac{5+3}{0-2} = \frac8{-2} = -4 \\[1em]
f'(x) &= 3x^2 - 8x \\
-4 &= 3x^2 - 8x \\
0 &= 3x^2 - 8x + 4 \\
&= 3x^2 - 2x - 6x + 4 \\
&= x(3x-2) - 2(3x-2) \\
&= (x-2)(3x-2) \\[1em]
x &= 2 \ \ \ \ \ \ \ x = \frac23
\end{aligned}
$$

### Together

a.

$$
\begin{aligned}
f'(x) &= \frac{-(-x^2+4x+12) - (-2x+4)(-x+7)}{(-x+7)^2} \\
f'(x) &= \frac{x^2 - 4x - 12 - 2x^2 + 14x + 4x - 28}{(-x+7)^2} \\
f'(x) &= \frac{-x^2 + 14x - 40}{(-x+7)^2} \\[1em]
f'(c) &= \frac{f(b) - f(a)}{b - a} \\
f'(c) &= 0 \\[1em]
0 &= \frac{-c^2+14c-40}{(-c+7)^2} \\
0 &= -c^2 + 14c - 40 \\
c &= 10 \ \ \ \ \ \ \ c = 4 \\
c &\not = 7
\end{aligned}
$$

b.

$$
\begin{aligned}
f(x)  &= -2x^2 -8x - 3 \\
f'(x) &= -4x - 8 \\
f'(c) &= \frac{f(b) - f(a)}{b - a} \\
f'(c) &= 0 \\[1em]
0 &= -4c-8 \\
c &= -2
\end{aligned}
$$