# TITD Section 4.2

### Conceptual

_Rolle's Theorem._ Let $f$ be continuous on a closed interval $[a, b]$ and differentiable on $(a, b)$, with $f(a) = f(b)$, then there is at least one point $c$ in $(a, b)$ such that:

$$
f'(c) = 0
$$

_Mean Value Theorem._ If $f$ is continuous on a closed interval $[a, b]$ and differentiable on $(a, b)$, then there is at least one point $c$ in $(a, b)$ such that:

$$
\begin{aligned}
f'(c) = \frac{f(b) - f(a)}{b - a}
\end{aligned}
$$

![](2020-10-12-22-32-01.png)

### Example

$$
\begin{aligned}
f(x) &= x^2 - 6x + 8 \\
f'(x) &= 2x - 6 \\[1em]

f'(c) &= \frac{f(b) - f(a)}{b - a} \\
f'(c) &= \frac{(5^2 - 6(5) + 8) - (2^2 - 6(2) + 8)}{5-2} \\
f'(c) &= 1 \\[1em]

2c - 6 &= 1 \\
c &= \frac72 = 3.5
\end{aligned}
$$

![](2020-10-12-23-23-06.png)