# Classwork Section 4.6

Source code used:

```go
package main

import (
	"fmt"
	"math"
)

func newton(fn, fnprime func(float64) float64, n float64) float64 {
	return n - (fn(n) / fnprime(n))
}

func fn(x float64) float64 {
	return math.Pow(x, 3) - 2*x - 5
}

func fnprime(x float64) float64 {
	return 3*math.Pow(x, 2) - 2
}

func main() {
	var n = 2.0
	for i := 0; i < 3; i++ {
		fmt.Println(i, n)
		n = newton(fn, fnprime, n)
	}
}
```

1.

$$
\begin{aligned}
f(x) &= x^3 - 2x - 5 \\
x_1 &= 2 \\
x_2 &= 2.1 \\
x_3 &\approx 2.09457 \\
x_4 &\approx 2.09455 \\
\end{aligned}
$$

2.

$$
\begin{aligned}
f(x) &= x^2 - 6 \\
x_1 &= 2.6 \\
x_2 &\approx 2.45385 \\
x_3 &\approx 2.44949 \\
x_4 &\approx 2.44949 \\
\end{aligned}
$$

3.

$$
\begin{aligned}
x_1 &= -1.5 \\
x_2 &\approx -2.75676 \\
x_3 &\approx -2.34282 \\
x_4 &\approx -2.05537 \\
\end{aligned}
$$

4.

$$
\begin{aligned}
f(x) &= x^2 - 7 \\
x_1 &= 2.8 \\
x_2 &= 2.65 \\
x_3 &\approx 2.64575 \\
x_4 &\approx 2.64575 \\
\end{aligned}
$$

5.

$$
\begin{aligned}
x_1 &= -1.7 \\
x_2 &\approx -1.95837 \\
x_3 &\approx -1.93475 \\
x_4 &\approx -1.93456 \\
\end{aligned}
$$

6.

$$
\begin{aligned}
f(x) &= x^4 - 2 \\
x_1 &= 1.4 \\
x_2 &\approx 1.23222 \\
x_3 &\approx 1.19141 \\
x_4 &\approx 1.18921 \\
\end{aligned}
$$

7.

$$
\begin{aligned}
x_1 &= 1.3 \\
x_2 &\approx 1.51184 \\
x_3 &\approx 1.48601 \\
x_4 &\approx 1.48558 \\
\end{aligned}
$$

8.

$$
\begin{aligned}
f(x) &= x^3 - 3 \\
x_1 &= 1.2 \\
x_2 &\approx 1.49444 \\
x_3 &\approx 1.44405 \\
x_4 &\approx 1.44225 \\
\end{aligned}
$$

9.

$$
\begin{aligned}
y &= \cos x \\
y &= x^3 + 2 \\
\cos x &= x^3 + 2 \\
x^3 - \cos x + 2 &= 0 \\
x_1 &= -1.4 \\
x_2 &\approx -1.21327 \\
x_3 &\approx -1.1742 \\
x_4 &\approx -1.17258 \\
\end{aligned}
$$

10.

$$
\begin{aligned}
f(x) &= x^5 - 3x^3 + 3 \\
f(x) &= -x^3 - 6x^2 - 9x - 5 \ \text{with} \ x_1 = -1.7 \\
x^5 - 3x^3 + 3 &= -x^3 - 6x^2 - 9x - 5 \\
x^5 - 2x^3 + 6x^2 + 9x + 8 &= 0 \\
\end{aligned}
$$
$$
\begin{aligned}
x_1 &= -1.7 \\
x_2 &\approx -2.13527 \\
x_3 &\approx -1.98887 \\
x_4 &\approx -1.94973 \\
\end{aligned}
$$