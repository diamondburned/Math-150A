# Classwork Section 4.7

### Ex 1.

$$
\begin{aligned}
f(x) &= 2x \\
F_n(x) &= x^2 + C \\
F_1(x) &= x^2 + 1 \\
F_2(x) &= x^2 + 2 \\
F_3(x) &= x^2 + 3
\end{aligned}
$$

### Ex 2.

![](2020-11-04-17-51-11.png)

### Ex 3.

A. $\int f(x) = 8x + C$

B. $\int g(x) = \frac32x^2 + C$

C. $\int f(t) = \frac13t^3 + C$

D. $\int f(\theta) = -\sin\theta + C$

E. $\int g(t) = -\frac1{2t} + C$

F. $\int h(t) = \ln|x| + C$

G. $\int r(t) = -\frac1r + C$

H. $\int f(x) = -\frac1{2x^2} + C$

I. $\int g(\theta) = -\cos\theta + C$

J. $\int h(x) = \frac13x^3 - \frac32x^2 + 4x + C$

K. $\int f(x) = e^x + C$

L. $\int v(t) = 12t - 16t^2$

### Ex 4.

Trick #1: Rewrite

$$
\begin{aligned}
&\int 5e^t + \cos^{-2} t \\
&= 5e^t + \sin^{-1} t
\end{aligned}
$$

Trick #2: Multiple or Distribute

$$
\begin{aligned}
&\int (3x - 1)^2 dx \\
&= \int 9x^2 - 6x + 1 \\
&= 3x^3 - 2x^2 + x
\end{aligned}
$$

Trick #3: Separate Fractions

$$
\begin{aligned}
&\int \frac{x^2 + 4x + 5}{x^5} \\
&= \int (x^2 + 4x + 5)(x^{-5}) \\
&= \int x^{-3} + 4x^{-4} + 5x^{-5} \\
&= -\frac12 x^{-2} - \frac43 x^{-3} - \frac56x^{-4} \\
&= -\frac1{2x^2} - \frac4{3x^3} - \frac5{6x^4}
\end{aligned}
$$

### Ex 5.

A.

$$
\begin{aligned}
f'(x) &= 2x - 1 \\
f(x) &= x^2 - x + C \\
f(2) &= (2)^2 - 2 + C = 5 \\
   C &= 3 \\
f(x) &= x^2 - x + 3
\end{aligned}
$$

B.

$$
\begin{aligned}
f'(x) &= 2 \cos x \\
f(x) &= 2 \sin x + C \\
f(\frac\pi2) &= 2 \sin (\frac\pi2) + C = -3 \\
C &= -5 \\
f(x) &= 2 \sin x - 5
\end{aligned}
$$

C.

$$
\begin{aligned}
f'(x) &= x^2 + x - 7 \\
f(x) &= \frac13 x^3 + \frac12 x^2 - 7x + C \\
f(3) &= \frac13 3^3 + \frac12 3^2 - 7(3) + C = 1 \\
C &= \frac{17}2 \\
f(x) &= \frac{x^3}3 + \frac{x^2}2 - 7x + \frac{17}2
\end{aligned}
$$

D.

$$
\begin{aligned}
f'(x) &= 3x^2 - 4x + 2 \\
f(x) &= x^3 - 2x^2 + 2x + C \\
f(-1) &= (-1)^3 - 2(-1)^2 + 2(-1) + C = -4 \\
C &= 1 \\
f(x) &= x^3 - 2x^2 + 2x + 1
\end{aligned}
$$

E.

$$
\begin{aligned}
g'(t) &= \sqrt{t} = t^{-\frac12} \\
g(t) &= 2t^{\frac12} + C \\
g(t) &= 2\sqrt{t} + C \\
g(9) &= 2\sqrt{9} + C = 6 \\
g(9) &= 0 \\
g(t) &= 2t^{\frac12} \\
\end{aligned}
$$

F.

$$
\begin{aligned}
f'(\theta) &= \sin \theta \\
f(\theta) &= -\cos \theta + C \\
f(0) &= -\cos 0 + C = 2 \\
C &= 3 \\
f(\theta) &= -\cos \theta + 3
\end{aligned}
$$