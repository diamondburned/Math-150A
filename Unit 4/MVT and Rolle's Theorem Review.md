# Assignment

2) Function is both continuous and differentiable.

$$
\begin{aligned}
y &= x^3 - 3x^2 + 1 \ [0, 2] \\[1em]
f(0) &= 1 \ (0, 1) \\
f(2) &= 8 - 12 + 1 = -3 \ (2, -3) \\[1em]
y' &= 3x^2 - 6x \\
0 &= 3x^2 - 6x \\
x &= 0 \ (0, 1) \\
x &= 2 \ (2, -3) \\
\end{aligned}
$$

*: Critical values happen to be the endpoints.

4)

$$
\begin{aligned}
y &= \cos(2x); \left[0, \frac{3\pi}4\right] \\[1em]
f(0) &= 1 \ \ (0, 1) \\
f\left(\frac{3\pi}4\right) &= 0 \ \ \left(\frac{3\pi}4, 0\right) \\
f'(x) &= -2 \sin 2x \\
0 &= -2 \sin 2x \\
0 &= \sin 2x \\
x &= 0, \frac\pi2 \\
f(0) &= 1 \\
f\left(\frac\pi2\right) &= -1 \\
(0, 1) &\ \text{max} \\
\left(\frac\pi2, -1\right) &\ \text{min}
\end{aligned}
$$

7) Function is continuous

$$
\begin{aligned}
y &= \frac{20}{x^2 + 5}; [-5, 1] \\[1em]
f(-5) &= \frac23 \ \ \left(-5, \frac23\right) \ \text{abs max}\\
f(1)  &= \frac{20}6 \ \ \left(1, \frac{20}6\right) \\[1em]
f(x) &= 20(x^2+5)^{-1} \\
f'(x) &= -20(x^2+5)^{-2}(2x) \\
f'(x) &= \frac{-20(2x)}{(x^2+5)^2} \\
x &= 0 \ \ (0, 4) \ \text{abs min}
\end{aligned}
$$

8) Not continuous at $x = -2$; we cannot use MVT.

9) Function is continuous and differentiable.

$$
\begin{aligned}
f'(c) &= \frac{f(b) - f(a)}{b - a} = 0 \\
f(b) &= \frac{6^2}{4(6)-8} = \frac{36}{24 - 8} = \frac94 \\
f(a) &= \frac94 \\
f'(x) &= \frac{2x(4x-8) - x^2(4)}{(4x-8)^2} \\
0 &= 8x^2 - 16x - 4x^2 \\
0 &= 4x^2 - 16x \\
0 &= 4(x-4) \\
x &\not = 0 \\
x &= 4 \\
\end{aligned}
$$

10) Not continuous at $x = 0$; we cannot use MVT.

12)

$$
\begin{aligned}
y &= -\frac{x^2}2 - 3x - \frac{13}2; [-6, -3] \\
f(-6) &= -\frac{13}2 \\
f(-3) &= -2 \\
\frac{f(-6) - f(-3)}{-6- (-3)} &= \frac{-\frac{13}{2}-\frac{4}{2}}{-3} \\
&= -\frac92 \cdot \frac13 = \frac32 \\
y' &= \frac{-2x}2 - 3 \\
\frac32 &= -x - 3 \\
\frac92 &= -x \\
x &= -\frac92
\end{aligned}
$$

14)

Prove these:

$$
\begin{aligned}
f(-2) &= f(6) \\
f'(0) &= 0
\end{aligned}
$$

$$
\begin{aligned}
f(-2) &= 0 \\
f(6)  &= 0 \\
f'(x) &= \frac{(-2x+4)(x+3) - (-x^2 + 4x + 12)}{(x+3)^2} \\
0 &= (-2x+4)(x+3) - (-x^2 + 4x + 12) \\
0 &= -x^2 - 6x \\
0 &= x(x+6) \\
x &= 0, x \not= 6
\end{aligned}
$$

15) Not continuous at $x=0$.