# TITD Section 4.7

$$
\begin{aligned}
&\int x^k dx = \frac1{k+1}x^{k+1} + C \ _{k \not= -1} \\
&\int Kdx = Kx + C \\
&\int Kg(x)dx = K \int g(x)dx \\
&\int \left[f(x) \plusmn g(x) \right]dx = \int f(x)dx \plusmn \int g(x)dx \\
&\int x^{-1}dx = \ln|x| + C \\
&\int e^{kx}dx = \frac1k e^{kx} + C
\end{aligned}
$$

1.

$$
\begin{aligned}
f'(x) &= 3x^2 \\
\int f'(x) &= \frac{3}{2+1} x^{2+1} + C\\
f(x) &= x^3 + C
\end{aligned}
$$

2.

$$
\begin{aligned}
g'(x) &= -\sin(t) \\
\int g'(x) &= \int -\sin(t) t' \\
\int g'(x) &= \cos(t) \int \frac{dt}{dx} \\
g(x) &= t \cos(t)
\end{aligned}
$$

3.

$$
\begin{aligned}
h'(x) &= \frac3x + 2x^{10} + 7e^x \\
\int h'(x) &= \int 3(x)^{-1} + \frac{2}{11} x^{11} + 7e^x + C \\
h(x) &= 3 \ln |x| + \frac{2}{11} x^{11} + 7e^x + C
\end{aligned}
$$

4.

$$
\begin{aligned}
f''(x) &= -2 + 12x - 12x^5 \text{ when } f(0) = 4 \text{ and } f'(0) = 12 \\
f'(x) &= -2x + 6x^2 - 2x^6 \\
f(x) &= -x^2 + 2x^3 - \frac27 x^7
\end{aligned}
$$