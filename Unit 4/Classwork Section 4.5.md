# Classwork Section 4.5

a.

$$
\begin{aligned}
C'(x) &= 2x - 1200 \\
0 &= 2x - 1200 \\
1200 &= 2x \\
x &= 600
\end{aligned}
$$

b.

$$
\begin{aligned}
h'(t) &= -9.8t + 60 \\
0 &= -9.8t + 60 \\
t &= \frac{-60}{-9.8} \\
t &= 6.12 \ \text{seconds} \\[1em]
h(6) &= -4.9(6)^2 + 60(6)+5 = 188.6
\end{aligned}
$$

c.

Let $p$ be the positive number and and $n$ be the negative number.

$$
\begin{aligned}
p &= 6 + n \\
(6 + n)n &= P(x) \\
6n + n^2 &= P(x) \\
6 + 2n &= P'(x) \\
6 + 2n &= 0 \\
n &= -3 \ \ \ \ \ \ \ \ \ \ p = 3 \\
\end{aligned}
$$

a.

Let $x$ be the length of the bent-upwards sides.

$$
\begin{aligned}
V(x) &= x \cdot (10-2x) \cdot (16-2x) \\
V(x) &= (10x - 2x^2) (16-2x) \\
V(x) &= 4x^3 - 32x^2 + 140x \\
V'(x) &= 12x^2 - 64x + 140 \\
V'(x) &= 12x^2 - 64x + 140 \\
\end{aligned}
$$

b.

Let $w$ and $h$ be the sides of the rectangle.

$$
\begin{aligned}
wh &= 1600 \\
h &= \frac{1600}w \\[1em]
P(w) &= 2\left(\frac{1600}w\right) + 2w \\
P(w) &= \frac{3200}w + 2w \\
P'(w) &= -\frac{3200}{w^2} + 2 \\

\end{aligned}
$$

c.

Let $p$ be the positive number.

$$
\begin{aligned}
S(p) &= p + \frac1p \\
S'(p) &= 1 - \frac1{p^2} \\
0 &= 1 - \frac1{p^2} \\
p &= 1
\end{aligned}
$$

d.

Let $p_1$ and $p_2$ be the two positive integers.

$$
\begin{aligned}
p_1 + p_2 &= 10 \\
p_2 &= 10 - p_1 \\[1em]

S(p_1) &= {p_1}^2 + (10 - p_1)^2 \\
S'(p_1) &= 2{p_1} - 2(10 - p_1) \\
0 &= 2p_1 - 2(10 - p_1) \\
0 &= 2p_1 +2p_1 - 20 \\
0 &= 4p_1 - 20 \\
p_1 &= \frac{20}4 \\
p_1 &= 5 \\
p_2 &= 10 - 5 = 5
\end{aligned}
$$

e.

$$
\begin{aligned}
P(x) &= R(x) - C(x) \\
P(x) &= 10x - 2x - x^2 \\
P'(x) &= 10 - 2 - 2x \\
0 &= -2x + 8 \\
x &= \frac8{-2} \\
x &= -4 \ \ \ \text{(?)}
\end{aligned}
$$

f.

$$
\begin{aligned}
V &= \pi r^2 h \\
16 \pi &= \pi r^2 h \\
16 &= r^2h \\
\frac{16}{r^2} &= h \\[1em]
SA &= 2 \pi r^2 + 2 \pi r h \\
SA &= 2 \pi r^2 + 2 \pi r \frac{16}{r^2} \\
SA' &= 4 \pi r - \frac{32 \pi }{r^2} \\
SA' &= \frac{4 \pi r^3 - 32\pi}{r^2} \\
0 &= 4 \pi r^3 - 32 \pi \\
r^3 &= 8 \\
r &= 2 \\
h &= \frac{16}{2^2} = 4
\end{aligned}
$$

g. Skip.

h.

$$
\begin{aligned}
d &= \sqrt{(x-1)^2 + (y-1)^2} \\
d &= \sqrt{(x-1)^2 + (5-2x-1)^2} \\
d &= \sqrt{(x-1)^2 + (4-2x)^2} \\
d &= \sqrt{x^2 - 2x + 1 + 16 - 16x + 4x^2} \\
d &= \sqrt{5x^2 - 18x + 17} \\
d' &= \frac1{2\sqrt{5x^2 - 18x + 17}} \cdot (10x - 18) \\
0 &= 10x - 18 \\
x^2 &= \frac{18}{10} \\
x^2 &= \frac95 \\
x &= \sqrt{\frac95}
\end{aligned}
$$

i.

$$
\begin{aligned}
d &= \sqrt{(x-2)^2 + (y-0)^2} \\
d &= \sqrt{(x-2)^2 + (x^2-0)^2} \\
d &= \sqrt{x^2 - 4x + 4 + x^4} \\
d' &= \frac1{2\sqrt{x^2 - 4x + 4 + x^4}} \cdot (2x - 4 + 4x^3) \\
0 &= 2x - 4 + 4x^3 \\
x &= 0.835
\end{aligned}
$$

## Review

### Implicit Differentiation

a.

$$
\begin{aligned}
x^2 - y^2 &= 4 \\
2x - 2y \dydx &= 0 \\
\dydx &= \frac{x}y
\end{aligned}
$$

b.

$$
\begin{aligned}
12x + 6y \dydx &= 0 \\
\dydx &= -\frac{12x}{6y} \\
      &= -\frac{2x}{y}
\end{aligned}
$$

c.

$$
\begin{aligned}
3x^3 + 9xy^2 &= 5x^3 \\
9x^2 + 9y^2 + 18xy \dydx &= 15x^2 \\
18xy \dydx &= 15x^2 - 9x^2 - 9y^2 \\
6xy \dydx &= 5x^2 - 3x^2 - 3y^2 \\
\dydx &= \frac{5x^2 - 3x^2 - 3y^2}{6xy}
\end{aligned}
$$

d.

$$
\begin{aligned}
y \sin(xy) &= y^2 + 2 \\
\dydx \sin(xy) + y \cos(xy) \dydx &= 2y \dydx \\
\dydx \sin(xy) + y \cos(xy) \dydx - 2y \dydx &= 0 \\
\dydx(\sin(xy) + y \cos(xy) - 2y) &= 0 \\
\dydx &= 0
\end{aligned}
$$

a.

$$
\begin{aligned}
y &= x^2 + 3 \\
\frac{dy}{dt} &= 2x \frac{dx}{dt} \\
\frac{dy}{dt} &= 2(1)(4) \\
\frac{dy}{dt} &= 8
\end{aligned}
$$

b.

$$
\begin{aligned}
y &= 2x^2 + 1 \\
\frac{dy}{dt} &= 4x \frac{dx}{dt} \\
1 &= 4(-2) \frac{dx}{dt} \\
\frac{dx}{dt} &= -\frac18
\end{aligned}
$$

c.

$$
\begin{aligned}
z^2 &= 1^2 + 2^2 \\
z &= \sqrt{5} \\[1em]

2z \frac{dz}{dt} &= 2x \frac{dx}{dt} + 2y \frac{dy}{dt} \\
2 (\sqrt5) \frac{dz}{dt} &= 2(1)(4) + 2(3)(3) \\
2 \sqrt5 \frac{dz}{dt} &= 26 \\
\frac{dz}{dt} &= \frac{26}{2\sqrt5}
              &= \frac{13}{\sqrt5}
\end{aligned}
$$

### Related Rates

a.

Know

$$
\begin{aligned}
a^2 + b^2 = 10^2
\end{aligned}
$$

Given

$$
\begin{aligned}
\frac{db}{dt} &= -2 \ \text{ft/sec}
\end{aligned}
$$

Find

$$
\begin{aligned}
\frac{da}{dt} \Bigl|_{a = 5}
\end{aligned}
$$

$$
\begin{aligned}
a &= 5 \\
5^2 + b^2 &= 10^2 \\
b &= 5 \sqrt3 \\[1em]
2a \frac{da}{dt} + 2b \frac{db}{dt} &= 0 \\
2(5) \frac{da}{dt} + 2(5\sqrt3)(-2) &= 0 \\
\frac{da}{dt} &= \frac{-2(5\sqrt3)(-2)}{2(5)} \\
\frac{da}{dt} &= 2\sqrt3 \ \text{ft / sec}
\end{aligned}
$$

b.

Know

$$
\begin{aligned}
V &= s^3
\end{aligned}
$$

Given

$$
\begin{aligned}
\frac{dV}{dt} = -10 \ \text{m/s}
\end{aligned}
$$

Find

$$
\begin{aligned}
\frac{ds}{dt} \Bigl|_{s = 2}
\end{aligned}
$$

$$
\begin{aligned}
\frac{dv}{dt} &= 3s^2 \frac{ds}{dt} \\
10 &= 3(2)^2 \frac{ds}{dt} \\
\frac{ds}{dt} &= \frac{10}{3(2)^2} \\
\frac{ds}{dt} &= \frac56
\end{aligned}
$$

c.

Know

$$
\begin{aligned}
V &= \frac13 \pi r^2 h
\end{aligned}
$$

Given

$$
\begin{aligned}
r = 3h
\end{aligned}
$$

Find

$$
\begin{aligned}
\frac{dh}{dt} \Bigl|_{h = 5}
\end{aligned}
$$

$$
\begin{aligned}
V &= \frac13 \pi (3h)^2 \cdot h \\
&= 3 \pi h^3 \\
\frac{dV}{dt} &= 9 \pi h^2 \frac{dh}{dt} \\
10 &= 9 \pi (5)^2 \frac{dh}{dt} \\
\frac{dh}{dt} &= \frac{10}{9 \pi \cdot 25} \\
\frac{dh}{dt} &= \frac2{45\pi}
\end{aligned}
$$

d.

Know

$$
\begin{aligned}
A &= \pi r^2
\end{aligned}
$$

Given

$$
\begin{aligned}
\frac{dr}{dt} = 2
\end{aligned}
$$

Find

$$
\begin{aligned}
\frac{dA}{dt} \Bigl|_{r = 5}
\end{aligned}
$$

$$
\begin{aligned}
\frac{dA}{dt} &= 2 \pi r \frac{dr}{dt} \\
\frac{dA}{dt} &= 2 \pi \cdot 5 \cdot 2 \\
\frac{dA}{dt} &= 20 \pi \ \text{m}^2\text{/sec}
\end{aligned}
$$

#### Together

a.

Know

$$
\begin{aligned}
a^2 + b^2 = c^2 \\
\end{aligned}
$$

Given

$$
\begin{aligned}
\frac{da}{dt} &= 16 \\
\frac{db}{dt} &= 12
\end{aligned}
$$

Find

$$
\begin{aligned}
\frac{dc}{dt} \Bigl|_{a = 4 \ \text{mi}}
\end{aligned}
$$

$$
\begin{aligned}
d &= rt \\
4 &= 16t \\
t &= \frac14 \\
d = b &= \frac{12}4 = 3 \\
3^2 + 4^2 &= c^2 \\
c &= 5 \\[1em]
2a \frac{da}{dt} + 2b \frac{db}{dt} &= 2c \frac{dc}{dt} \\
2(4) (16) + 2 (3) (12) &= 2 (5) \frac{dc}{dt} \\
\frac{dc}{dt} &= 20 \ \text{mph}
\end{aligned}
$$

b.

Know

$$
\begin{aligned}
\frac{\text{big}}{\text{little}} &= \frac{10}6 &= \frac{s+x}{x}
\end{aligned}
$$

Given

$$
\begin{aligned}
\frac{dx}{dt} &= 3
\end{aligned}
$$

Find

$$
\begin{aligned}
\frac{ds}{dt} \Bigl|_{s = 10}
\end{aligned}
$$

$$
\begin{aligned}
10x &= 6s + 6x \\
4x &= 6s \\
4 \frac{dx}{dt} &= 6 \left(\frac{ds}{dt}\right) \\
4 (3) &= 6 \left(\frac{ds}{dt}\right) \\
\frac{ds}{dt} &= 2 
\end{aligned}
$$

### Linear Approximation

1. 

$$
\begin{aligned}
y &= x^3 \ (1, 1) \\
y &= m(x-1)+1 \\
m &= 3x^2 = 3(1)^2 = 3 \\
(1.01)3 &= 3(1.01-1)+1 \\
&\approx 3(0.01)+1 - 1.03
\end{aligned}
$$

2.

$$
\begin{aligned}
y &= x^{-3} \ (1, 1) \\
y &= m(x-1)+1 \\
\end{aligned}
$$

3.

$$
\begin{aligned}
y &= \sqrt{x} \ (9, 3) \\
y &= m(x-9)+3
\end{aligned}
$$

### Differentials

4.

$$
\begin{aligned}
\frac{dy}{dx} &= 6x-1 \\
dy &= (6x-1)(dx) \\
dy &= (6(2)-1)(0.01) \\
dy &= 11(0.01) = 1.1
\end{aligned}
$$

5.

$$
\begin{aligned}
\dydx &= \frac{-1}{(x+1)^2} \\
dy &= \frac{-dx}{(x+1)^2}   \\
dy &= \frac{-0.25}{(1+1)^2} \\
dy &= \frac{-0.25}{4} \\
dy &= -0.0625
\end{aligned}
$$

6.

$$
\begin{aligned}
y &= \frac{\sin 2x}{x} \\
\dydx &= \frac{\sin 2x - 2x (\cos 2x)}{x^2} \\
dy &= \frac{dx \sin 2x - dx 2x (\cos 2x)}{x^2} \\
dy &= \frac{0.25 \sin 2\pi - 2\pi(0.25) (\cos 2\pi)}{\pi^2} \\
dy &= -\frac{0.5\pi}{\pi^2} \\
dy &= -\frac\pi2
\end{aligned}
$$