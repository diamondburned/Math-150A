# TITD Section 4.8

### Newton's Method Formula

$$
\begin{aligned}
x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)}
\end{aligned}
$$

### Examples

$$
\begin{aligned}
f(x) &= x^2 - 8 \\
f'(x) &= 2x \\[1em]
x_1 &= 3
\end{aligned}
$$

#### Iteration 1

$$
\begin{aligned}
x_2 &= 3 - \frac{3^2 - 8}{2(3)} \\
x_2 &= 3 - \frac{19}{6} \\
x_2 &= -\frac16
\end{aligned}
$$
#### Iteration 2

$$
\begin{aligned}
x_3 &= -\frac{289}{12} \approx -24.083
\end{aligned}
$$

#### Iteration 3

$$
\begin{aligned}
x_4 &\approx -12.208
\end{aligned}
$$

![](2020-10-23-18-30-39.png)

$x=3$ is a reasonable guess because it is very close to $y = 0$.