# Classwork Section 4.4

1. 
   
1.1. Polynomial function

1.2.

$$
\begin{aligned}
\limfn{x}{\infty}f(x) &= \infty \\
\limfn{x}{-\infty}f(x) &= \infty
\end{aligned}
$$

1.3. Domain: all reals

1.4. 

y-intercepts: $(0, 2)$

x-intercepts: $(2, 0)$

1.5.

$$
\begin{aligned}
f'(x) &= 2x - 4 \\
0 &= 2x - 4 \\
x &= 2
\end{aligned}
$$

|   1   |   2   |   3   |
| :---: | :---: | :---: |
|   ↘   |   →   |   ↗   |

Critival Values at (0, 0).

This function is increasing over thei tnerval $(2, \infty)$ and decreasing over the interval $(-\infty, 2)$.

Inflection points: $f'(x) = 1$.

This function is always concave up

2.

2.1. Rational

2.2. 

$$
\begin{aligned}
\limfn{x}{-\infty} &= \infty \\
\limfn{x}{ \infty} &= -\infty
\end{aligned}
$$

2.3. Domain: all reals

2.4.

y-intercepts: $(0, 0)$

x-intercepts: $(0, 0)$, $(-3, 0)$

2.5.

$$
\begin{aligned}
f(x) &= \frac{-2x^3-6x^2}{12} \\
f'(x) &= \frac{-(12)(-6x^2 - 12x)}{12^2} \\
f'(x) &= \frac{72x^2 + 144x}{12^2} \\
0 &= 72x^2 + 144x \\
x &= -2 \ \ \ \ \ \ \ \ \ \ x = 0
\end{aligned}
$$

Critival Values at $\left(-2, -\frac23\right)$ and $(0, 0)$.

|  -3   |  -2   |  -1   |   0   |   1   |
| :---: | :---: | :---: | :---: | :---: |
|   ↗   |   →   |   ↘   |   →   |   ↗   |

The function is increasing over the interval $(-\infty, -2) \cup (0, \infty)$ and decreasing over the interval $(-2, 0)$.

$$
\begin{aligned}
f''(x) &= \frac{2(12)(72x^2+144x) - (12^2)(144x+144)}{12^4} \\
f''(x) &= \frac{1728x^2 + 3456 - 20736x - 20736}{12^4} \\
\end{aligned}
$$

3.

3.1. Polynomial

3.2. 

$$
\begin{aligned}
\limfn{x}{-\infty} &= -\infty \\
\limfn{x}{\infty} &= -\infty
\end{aligned}
$$

3.3. Domain: all reals

3.4. 

y-intercepts: $(0, -1)$ 

x-intercepts: $(-1, 0)$, $(1, 0)$

3.5.

$$
\begin{aligned}
f'(x) &= -4x^3 + 4x \\
0 &= -4x^3 + 4x \\ 
x &= 0 \ \ \ \ \ \ \ x = \plusmn 1
\end{aligned}
$$

Critical values at $(-1, 0)$, $(0, -1)$ and $(1, 0)$

|  -2   |  -1   | -0.5  |   0   |  0.5  |   1   |   2   |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|   ↗   |   →   |   ↘   |   →   |   ↗   |   →   |   ↘   |

The function is increasing over the interval $(-\infty, -1) \cup (0, 1)$ and decreasing over the interval $(-1\ 0) \cup (1, \infty)$.

$$
\begin{aligned}
f''(x) &= -12x^2 + 4 \\
0 &= -12x^2 + 4 \\
\frac13 &= x^2
\end{aligned}
$$

4.

4.1. Rational

4.2.

$$
\begin{aligned}
\limfn{x}{\infty} &= -3 \\ 
\limfn{x}{-\infty} &= -3
\end{aligned}
$$

4.3. Domain: $(-\infty, 2) \cup (2, \infty)$

4.4. $(0, 0)$

4.5.

|   1   |   2   |   3   |
| :---: | :---: | :---: |
|   ↗   |   →   |   ↗   |

The function is increasing over the interval $(-\infty, 2) \cup (2, \infty)$

|     1     |   2   |     3     |
| :-------: | :---: | :-------: |
| $\bigcup$ |   -   | $\bigcap$ |

The function is concave up over the interval $(-\infty ,2)$ and concave down over the interval $(2, \infty)$.