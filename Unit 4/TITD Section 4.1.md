# TITD Section 4.1

### Conceptual

![](2020-10-13-14-05-29.png)

### Practice

$$
\begin{aligned}
f(x) &= x^{3/5} (4-x) \\
f(x) &= 4x^{3/5} - x^{8/5} \\
f'(x) &= \frac{12}5x^{-2/5} - \frac85 x^{3/5} \\
f'(x) &= \frac{12}{5(\sqrt[5]{x^2})} - \frac{8(\sqrt[5]{x^3})}{5} \\
f'(x) &= \frac{12-8(\sqrt[5]{x^3})(\sqrt[5]{x^2})}{5(\sqrt[5]{x^2})} \\
f'(x) &= \frac{12-8x}{5(\sqrt[5]{x^2})} \\
0 &= \frac{12-8x}{5(\sqrt[5]{x^2})} \\
0 &= 12 - 8x \\
x &= \frac32 \\
x & \not = 0
\end{aligned}
$$

The critical value is $\frac32$.

![](2020-10-13-14-27-00.png)

### Example

$$
\begin{aligned}
f(x) &= x^3 \\
f'(x) &= 3x^2 \\
0 &= 3x^2 \\
x &= 0 \\[1em]
f(-1) &= -1 \\
f(0)  &= 0 \\
f(1)  &= 1
\end{aligned}
$$

Because $f(-1) < f(0) < f(1)$, the point at $x = 0$ is neither a local minimum nor a local maximum.