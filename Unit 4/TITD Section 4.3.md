# TITD Section 4.3

### Conceptual

If $f'(x) > 0$ for all x-values over a given interval, then $f$ is _increasing_ over that interval.

If $f'(x) < 0$ for all x-values over a given interval, then $f$ is _decreasing_ over that interval.

If $f''(x) > 0$ for all x-values over a given interval, then $f$ is _concave up_ over that interval.

If $f'(x) < 0$ for all x-values over a given interval, then $f$ is _concave down_ over that interval.

### Example

Step 1:

$$
\begin{aligned}
g(x) &= x^4 - x^5 \\
g'(x) &= 4x^3 - 5x^4 \\
0 &= 4x^3 - 5x^4 \\
0 &= x^3(4 - 5x) \\
x &= 0 \ \ \ \ \ \ \ \ x = \frac45
\end{aligned}
$$

Step 2:

| $-1$ | $0$ | $\frac25$ | $\frac45$ | $1$ |
| ---- | --- | --------- | --------- | --- |
| ↘    | →   | ↗         | →         | ↘   |

### Example (sketch)

![](2020-10-16-22-03-45.png)