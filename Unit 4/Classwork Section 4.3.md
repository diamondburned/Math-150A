# Classwork Section 4.3

### Groups

a. $(c, d)$

b. $(a, b)$, $(b, c)$, $(d, e)$, $(e, f)$

c.

Relative minimum: $c$

Relative maximum: $d$, $a$

d. $f'(x) > 0$: $(c, d)$

e. $f'(x) < 0$: $(a, c)$, $(d, e)$, $(e, f)$

f. $f'(x) = 0$: $e$, $a$, $c$

g. $f'(x)$ does not exist: $d$

h.

Concave down: $(a, b)$, $(e, f)$

Concave up: $(b, d)$, $(d, e)$

### Together

a.

$$
\begin{aligned}
f(x) &= x^4 - 8x^2 + 1 \\
f'(x) &= 4x^3 - 16x \\
&= 4x(x^2-4) \\
0 &= 4x(x-2)(x+2) \\
x &= -2, 0, 2
\end{aligned}
$$

| $-3$  | $-2$  | $-1$  |  $0$  |  $1$  |  $2$  |  $3$  |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|   ↘   |   →   |   ↗   |   →   |   ↘   |   →   |   ↗   |

Relative minimum: $(2, -15)$, $(-2, -15)$

Relative maximum: $(0, 1)$

b.

$$
\begin{aligned}
f(x) &= (x^2-4)^\frac23 \\
f'(x) &= \frac23(x^2-4)^{-\frac13} \cdot 2x \\
&= \frac23 \frac{2x}{(x^2-4)^\frac13}
\end{aligned}
$$

$x=0$ and $x\not=\plusmn2$ are critical values.

| $-3$  | $-2$  | $-1$  |  $0$  |  $1$  |  $2$  |  $3$  |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|   ↘   |   →   |   ↗   |   →   |   ↘   |   →   |   ↗   |

Relative minimum: $(-2, 0)$, $(2, 0)$

Relative maximum: $(0, \sqrt[3]{8})$

### Together

a.

$$
\begin{aligned}
f(x) &= x^4 - 4x^2 \\
f'(x) &= 4x^3 - 8x \\
f''(x) &= 12x^2 - 8 \\
0 &= 12x^2 - 8 \\
8 &= 12x^2 \\
\frac8{12} &= x^2 \\
x &= \plusmn \sqrt\frac23 \\
\end{aligned}
$$

| -1        | $-\sqrt\frac23$ | 0         | $\sqrt\frac23$ | 1         |
| --------- | --------------- | --------- | -------------- | --------- |
| $\bigcup$ | -               | $\bigcap$ | -              | $\bigcup$ |


Concave up: $\left(-\infty, -\frac23\right)$, $\left(\frac23, \infty\right)$

Concave down: $\left(-\frac23, \frac23\right)$

b.

$$
\begin{aligned}
f(x) &= -3x^5 + 5x^3 \\
f'(x) &= -15x^4 + 15x^2 \\
f''(x) &= -60x^3 + 30x \\
&= -30x(2x^2-1) \\
x &= 0 & 2x^2 - 1 &= 0 \\
  &    & x        &= \plusmn\sqrt\frac12
\end{aligned}
$$

|    -1     | $-\sqrt\frac12$ | $-\sqrt\frac14$ |   0   | $\sqrt\frac14$ | $\sqrt\frac12$ |     1     |
| :-------: | :-------------: | :-------------: | :---: | :------------: | :------------: | :-------: |
| $\bigcup$ |        -        |    $\bigcap$    |   -   |   $\bigcup$    |       -        | $\bigcap$ |

Concave up: $(-\infty, -\sqrt12) \cup (0, \sqrt12)$

Concave down: $(-\sqrt12, 0)$, $(\sqrt12, \infty)$