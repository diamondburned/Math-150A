# Classwork Section 4.1

### Together

A.

Abs. max. $(4, 5)$ endpoint

Abs. min. $(2, 1)$ interval

B.

Abs. max. none

Abs. min. $(-1, -1)$ endpoint

C.

Abs. max. none

Abs. min. none

D.

Abs. max. $(3, 2)$ (interval; derivative undefined)

Abs. min. none

### Defining Relative and Absolute Extrema of a Function

Relative Extrema - points (defined) on the graph of the function where the function changes from increasing to decreasing (and vice versa).

Absolute Extrema - the highest or lowest point (defined) on the graph or the function over a specified domain.

### Groups

A.

Relative extrema of $f(x)$: $(-1.5, -6)$, $(1.5, 6)$.

Function $f$ does not have an absolute extrema.

B.

Relative extrema of $g(x)$: $(-5, 4)$, $(-3, -3)$, $(-1, 1)$, $(1, -5)$, $(3, 5)$, $(5, 2)$

Function $g$ has abslute extremas at $(1, -5)$ and $(3, 5)$

C. In both $f$ and $g$, the extremas are within the interval. $f$ did not have an absolute extrema, and $g$'s relative extremas occur at its absolute extremas.

### Groups

A.

Absolute extremas of $f(x)$ within $[-2, 2]$: $(-1.5, -6)$, $(1.5, 6)$.

Absolute extremas of $f(x)$ within $[-1, 2]$: $(-1, -3)$, $(1.5, 6)$.

B.

Absolute extremas of $g(x)$ within $[-3, 3]$: $(1, -5)$ and $(3, 5)$

Absolute extremas of $g(x)$ within $[-5, 2]$: $(-5, 4)$ and $(1, -5)$

C. Absolute extremas can occur at either closed endpoints, where $f'(x) = 0$, or where $f'(x)$ is undefined.

### Extreme Value Theorem

### Together

**A.**

Endpoints: 

$$
\begin{aligned}
f(-4) &= \frac{11}6 \\
f(4) &= \frac52 \\[1em]
f'(x) &= \frac{2(x-2)-(2x-3)}{(x-2)^2} & x = 2 \\
0 &= 2x - 4 -2x + 3 \\
0 &= -1 \ \text{(false)}
\end{aligned}
$$

The function is not continuous because $x = 2$ is within the interval and is undefined.

**B.**

$f(x)$ is continuous at $[-5, 5]$

**C.**

$f(x)$ is not continuous.

### Groups

a.

$f(x)$ is continuous.

$$
\begin{aligned}
f(-1) &= 0 \\
f(2) &= 36 \\[1em]
f'(x) &= 24x^2 - 6x - 9 \\
0 &= 24x^2 - 6x - 9 \\
0 &= 8x^2 - 2x - 3 \\
0 &= (4x-3)(2x+1) \\
x &= -\frac12 \ \ \ \ \ x = \frac34 \\[1em]
f(-\frac12) &= -4.75 \\
f(\frac34) &= -3.063
\end{aligned}
$$

Abs. min. $(\frac34, -3.063)$

Abs. max. $(2, 36)$

b.

$$
\begin{aligned}
g(0) &= 0 \\
g(2\pi) &= 2\pi \approx 6.28 \\[1em]
g'(x) &= 1 -2 \cos x \\
0 &= 1 -2 \cos x \\
1 &= 2 \cos x \\
\cos x &= \frac12 \\
x &= \frac\pi3 \ \ \ \ \ x = \frac{5\pi}3 \\[1em]
g\left(\frac\pi3\right) &= \frac\pi3 - \sqrt3 \approx -0.68 \\
g\left(\frac{5\pi}3\right)  &= \approx 6.948
\end{aligned}
$$

Abs. min. $(\frac\pi3, \frac\pi3 - \sqrt3)$

Abs. max. $(2\pi, 6.28)$

c.

$$
\begin{aligned}
k(0) &= 7 \\
k(4) &= 175 \\[1em]
k'(x) &= 12x^2 + 10x - 42 \\
0 &= 12x^2 + 10x - 42 \\
x &= \frac32 \ \ \ \ \ x = -\frac73 \\
k\left(\frac32\right) &= -31.25 \\
k\left(-\frac73\right) &\approx 81.41
\end{aligned}
$$

Abs. min. $(\frac32, -31.25)$

Abs. max. $(4, 175)$

d.

EUT does not apply because $f(x)$ is not continuous within $[-2, 5]$.

e.

$$
\begin{aligned}
h(1) &= 1 \\
h(10) &= 4 \\[1em]
h'(x) &= \frac23(x-2)^{-\frac13} \\
0 &= \frac23(x-2)^{-\frac13} \\
0 &= x - 2 \\
x &= 2 \\[1em]
h(2) &= 0
\end{aligned}
$$

Abs. min. $(2, 0)$

Abs. max. $(10, 4)$

f.

$$
\begin{aligned}
g\left(-\frac\pi6\right) &= \frac12 \\
g(0) &= 1 \\[1em]
g'(x) &= -2\sin (2x) \\
0 &= \sin(2x) \\
2x &= 0 \\
x &= 0 \\[1em]
g(0) &= 1
\end{aligned}
$$

Abs. min. $(-\frac\pi6, \frac12)$

Abs. max. $(0, 1)$