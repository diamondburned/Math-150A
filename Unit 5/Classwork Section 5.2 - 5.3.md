# Classwork Section 5.2 - 5.3

The notation $\int_a^b f(x) dx$ reads as "the definite integral from $a$ to $b$ on $f(x)$ with respect to $x$.

![](2020-11-09-08-44-46.png)

The definite integral can be used to determine the net area.

Ex #1.

$$
\begin{aligned}
&1. &\int_0^7      f(x) dx &= 20 \\
&2. &\int_0^{13}   f(x) dx &= 20 - 13.5 = 6.5 \\
&3. &\int_0^{-6}   f(x) dx &= -\frac12(\pi \cdot 3^2) = -\frac{9\pi}2 _{r=3} \\
&4. &\int_3^3      f(x) dx &= 0 \\
&5. &\int_7^0      f(x) dx &= -20 \\
&6. &\int_{10}^4   f(x) dx &= -(8 - 7.5) = -0.5 \\
&7. &\int_{-8}{13} f(x) dx &= 4 - \pi{9\pi}2 + 6.5 = 10.5 - \frac{9\pi}2 \\
\end{aligned}
$$

11. True

Ex #2.

$$
\begin{aligned}
&\text{A}.& \int_b^a g(x) dx & = 3 \\
&\text{B}.& \int_a^b 2 \cdot f(x) dx &= 16 \\
&\text{C}.& \int_a^b 5 \ dx &= 5(b-a) \\
&\text{D}.& \int_a^b [5 + g(x)] dx &= 5(b-a) - 3 \\
&\text{E}.& \int_a^b [f(x) - h(x)] dx &= 8 - \frac52 = \\
&\text{F}.& \int_a^4 h(x) dx + \int_4^b h(x) dx &= \int_a^b h(x) dx = \frac52
\end{aligned}
$$

Ex #3.

$$
\begin{aligned}
\int_1^5 3 \cdot f(x) + 4 \ dx &= 34 \\
3 \int_1^5 f(x) dx + \int_1^5 4 \ dx &= 34 \\
3 \int_1^5 f(x) dx + 4(5-1) \ dx &= 34 \\
3 \int_1^5 f(x) dx + 16 &= 34 \\
3 \int_1^5 f(x) dx &= 18 \\
\int_1^5 f(x) dx &= 6 \\
\end{aligned}
$$

Ex #4. If f(x) is:

<br>

A. an even funcion & $\int_0^{-3} f(x) dx = 8$ => $\int_{-3}^3 f(x) dx$

![](2020-11-09-09-17-59.png)

$$
\begin{aligned}
\int_{-3}^3 f(x) dx &= 16 \\
\end{aligned}
$$

B. an odd function & $\int_0^3 f(x) dx = 8$ => $\int_{-3}^3 f(x) dx$

![](2020-11-09-09-18-10.png)

$$
\begin{aligned}
\int_{-3}^3 f(x) dx &= 0
\end{aligned}
$$

Ex #5.

![](2020-11-09-09-21-31.png)

$$
\begin{aligned}
A &= \frac12 (3) (4+7) \\
  &= \frac32 (11) \\
  &= \frac{33}2 \\[1em]

& \left[\frac{x^2}2 + 3x \right]_1^4 \\
A = &\left[\frac{4^2}2 + 3(4)\right] - \left[\frac12 + 3\right] \\
A = &20 - \frac72 \\
A = &\frac{33}2
\end{aligned}
$$

B.

$$
\begin{aligned}
A &= \frac{5+1}2 \cdot 4 &= 12 \\
\end{aligned}
$$

C.

![](2020-11-09-09-33-00.png)

$$
\begin{aligned}
A &= \frac12 \pi (2)^2 = 2\pi
\end{aligned}
$$

D.

![](2020-11-09-09-34-12.png)

$$
\begin{aligned}
& A \cap + A \Box\ \\
&= \frac\pi2 (3)^2 + 3 \\ 
&= \frac{9\pi}4    + 3 \\ 
\end{aligned}
$$

E.

$$
\begin{aligned}
&\int_{-2}^4 |x| dx + \int_{-2}^4 3 \ dx \\
&= 2 + 8 + 3(4+2) \\
&= 10 + 18 = 28
\end{aligned}
$$

F. (odd function)

$$
\begin{aligned}
&\int_{-4}^4 \sqrt[3]{x} dx &= 0
\end{aligned}
$$

G.

$$
\begin{aligned}
&\int_1^4 (3x+2) dx \\
&= \int_1^4 3x \ dx + \int_1^4 2 \ dx \\
&= \left[\frac{3x^2}2\right]_1^4 + 2(4-1) \\
&= \frac{3(16)}2 - \frac32 + 6 \\
&= \frac{45}2 + 6
\end{aligned}
$$

H.

![](2020-11-09-09-47-57.png)

$$
\begin{aligned}
&\int_0^2 (1 + \sqrt{4-x^2}) dx \\
A &= \frac14 \pi r^2 + 2 \\
  &= \pi + 2 \\[1em]

&\int_2^5 (3-x) dx \\
&= \left[ 3x - \frac12 x^2 \right]_2^5 \\
&= 15 - \frac{25}2 - (6-2) \\
&= 11 - \frac{25}2 \\
&= -\frac32 \\[1em]

&\int_0^2 (1+\sqrt{4-x^2}) dx + \int_2^5 (3-x) dx \\
&= \pi + 2 - \frac32 \\
&= \pi + \frac12
\end{aligned}
$$