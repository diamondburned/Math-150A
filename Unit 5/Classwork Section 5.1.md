# Classwork Section 5.1

## Part 1

### A

$$
\begin{aligned}
A &= (7 \cdot 1) + (8 \cdot 1) + (7 \cdot 1) + (4 \cdot 1) \\
A &= 26 \text{ unit}^2
\end{aligned}
$$

### B

$$
\begin{aligned}
A &= (2 \cdot 1) + (5 \cdot 1) + (6 \cdot 1) + (5 \cdot 1) \\
A &= 18 \text{ unit}^2
\end{aligned}
$$

### C

$$
\begin{aligned}
A &= 3 + 8 + 11 + 12 + 11 + 8 \\
A &= 53 \text{ unit}^2
\end{aligned}
$$

### D

$$
\begin{aligned}
A &= 9 + 10 + 9 + 6 \\
A &= 34
\end{aligned}
$$

## Part 2

### A

$$
\begin{aligned}
h_1 &= f(-2.5) &= 3.75 \\
h_2 &= f(-1.5) &= 4.25 \\
h_3 &= f(-0.5) &= 4.75 \\
h_4 &= f(0.5)  &= 5.25 \\
A &= 3.75 + 4.25 + 4.75 + 5.25 \\
A &= 18
\end{aligned}
$$

### B

$$
\begin{aligned}
h_1 &= 2.375 \\
h_2 &= 4.375 \\
h_3 &= 5.375 \\
h_4 &= 5.375 \\
A &= 2.375 + 4.375 + 5.375 + 5.375 = 17.5
\end{aligned}
$$

### C

$$
\begin{aligned}
h_1 &= 5.5 \\
h_2 &= 4.5 \\
h_3 &= 3.5 \\
h_4 &= 2.5 \\
A &= 5.5 + 4.5 + 3.5 + 2.5 = 16
\end{aligned}
$$

### D

$$
\begin{aligned}
h_1 &= 5.75  \\
h_2 &= 9.75  \\
h_3 &= 11.75 \\
h_4 &= 11.75 \\
A &= 5.75 + 9.75 + 11.75 + 11.75 = 39
\end{aligned}
$$

## Part 3

### A

$$
\begin{aligned}
A_1 &= \frac{6 + 5}2 \cdot 2 = 11 \\
A_2 &= \frac{5 + 4}2 \cdot 2 = 9  \\
A_3 &= \frac{4 + 3}2 \cdot 2 = 7  \\
A_4 &= \frac{3 + 2}2 \cdot 2 = 5  \\
A &= 11 + 9 + 7 + 5 = 32
\end{aligned}
$$

### B

$$
\begin{aligned}
A_1 &= \frac{12 + 13}2 \cdot 1 = 12.5 \\
A_2 &= \frac{13 + 12}2 \cdot 1 = 12.5 \\
A_3 &= \frac{12 +  9}2 \cdot 1 = 10.5 \\
A_4 &= \frac{9 +   4}2 \cdot 1 = 6.5  \\
A &= 12.5 + 12.5 + 10.5 + 6.5 = 42 \\
\end{aligned}
$$

### C

$$
\begin{aligned}
A_1 &= \frac{8.5 + 8}2 \cdot 1 = 8.25 \\
A_2 &= \frac{8 + 7.5}2 \cdot 1 = 7.75 \\
A_3 &= \frac{7.5 + 7}2 \cdot 1 = 7.25 \\
A_4 &= \frac{7 + 6.5}2 \cdot 1 = 6.75 \\
A &= 8.25 + 7.75 + 7.25 + 6.75 = 30
\end{aligned}
$$

### D

$$
\begin{aligned}
A_1 &= \frac{5 + 5.5}2 \cdot 1 = 5.25 \\
A_2 &= \frac{5.5 + 5}2 \cdot 1 = 5.25 \\
A_3 &= \frac{5 + 3.5}2 \cdot 1 = 4.25 \\
A_4 &= \frac{3.5 + 1}2 \cdot 1 = 2.25 \\
A &= 5.25 + 5.25 + 4.25 + 2.25 = 17
\end{aligned}
$$