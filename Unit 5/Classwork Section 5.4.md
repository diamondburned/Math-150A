# Classwork Section 5.4

### Ex #1.

A.

Units of

$$
\begin{aligned}
f(x)&: \$ / \text{day} \\
d(t)&: \text{days} \\
f(t) dt&: \$
\end{aligned}
$$

$\int_1^{31} f(t) dt$ represents the total change in value of the 401K in dollars for the month of January.

B.

$\int_5^{20} r(t) dt$ represents the total change in temperature of pizza in $^oF$ time 5 minutes to 20 minutes.

C.

1. $\int_1^5 A(t) dt$ represents the number of students arriving from 1 to 5 minutes.

2. $\int_1^5 P(t) dt$ represents the number of tickets purchased from 1 to 5 minutes.

3. $A(t) - P(t)$ is the rate of change of the waiting line.

4. $\int_1^5 A(t) - P(t) dt$ represents the total change in length of the waiting line.

D.

1. Velocity is the rate at which position is hcanging.
2. $\int_5^{50} v(t) dt$ represents the total change in position on $[5, 50]$ which has a special name in math called displacement.
3. Likewise, $a(t)$ is the rate at twhich velocity is changing.

### Ex #2.

A. 

$$
\begin{aligned}
\int x^3 dx &= \frac14 x^4 \\
\int_1^3 x^3 dx &= 20
\end{aligned}
$$

B.

$$
\begin{aligned}
\int (x^2 - 3) dx &= \frac13x^3 - 3x \\
\int_1^2 (x^2 - 3) dx &= \frac{20}3
\end{aligned}
$$

C.

$$
\begin{aligned}
\int 3 \sqrt{x} dx &= 3 \int x^{\frac12} = 2x ^{\frac32} \\
\int_1^4 3\sqrt{x} dx &= 14
\end{aligned}
$$

D.

$$
\begin{aligned}
\int \sec^2 x dx &= \tan x \\
\int_0^{\frac\pi4} \sec^2 x dx &= 1
\end{aligned}
$$

E.

$$
\begin{aligned}
& \int_{-1}^1 (2x-1)^2 dx \\
&= \left[\frac13 (2x - 1)^3\right]_{-1}^1 \\
&= \left[\frac13 (2(1)-1)^3\right] - \left[\frac13 (2(-1)-1)^3\right] \\
&= \frac{28}3
\end{aligned}
$$

F.

$$
\begin{aligned}
& \int_0^\pi (\sin x + 1) dx \\
&= \int_0^\pi \sin x dx + \pi \\
&= \left[-\cos x \right]_0^\pi + \pi \\
&= -\cos \pi + \cos 0 + \pi \\
&= 2 + \pi
\end{aligned}
$$

G.

$$
\begin{aligned}
& \int_1^8 \sqrt{\frac2x} dx \\
&= \left[\ln |x|\right]_1^8  \\
&= \ln 8 - \ln 1 \\
\end{aligned}
$$

H.

$$
\begin{aligned}
& \int_1^4 \frac{x-2}{\sqrt{x}} dx \\
&= \int_1^4 x^{\frac12} dx - 2 \int_1^4 x ^{-\frac12} dx \\
&= \left[\frac23 x ^{\frac32}\right]_1^4 - 2\left[2x^{\frac12} \right]_1^4 \\
&= \frac{16}3 - \frac23 - 2[4-2] \\
&= \frac{14}3 - 4
\end{aligned}
$$

### Ex #3.

Case #1.

$$
\begin{aligned}
F(x) &= \sin t \bigl|_\pi^x \\
     &= \sin x - \sin\pi \\
     &= \sin x + 0 \\
F'(x) &= \cos x
\end{aligned}
$$

Case #2.

$$
\begin{aligned}
F(x) &= \int_0^{x-3} \sqrt{1+t} dt \\
&= \frac23 (1+t) ^{\frac32} \Bigl|_0^{x-3} \\
&= \frac23 \left[1 + (x-3)\right] ^{\frac32} - \frac23 (1+0)^{\frac32} \\
&= \frac23 \left[1 + (x-3)\right] ^{\frac32} - \frac23 \\
F'(x) &= \frac32 \left[\frac23 \left(1 + (x-3)\right) ^{\frac12}\right] \\
F'(x) &= \sqrt{1+(x-3)}
\end{aligned}
$$

Case #3.

$$
\begin{aligned}
F(x) &= \left[\frac{t^3}{3}\right]_{4\pi}^{\cos x} \\
&= \frac{\cos^3 x}3 - \frac{(4\pi)^3}3 \\
F'(t) &= \frac{3(\cos^2 x)(-\sin x)}{3} + 0 \\
F'(x) &= \cos^2 x \cdot (-\sin x)
\end{aligned}
$$

### Ex #4.

$$
\begin{aligned}
F(x) &= \int_0^{x^2} e^{t^2} dt \\
F'(x) &= e^{x^4} \cdot 2x
\end{aligned}
$$

#### Part 2

If $F(x) = \int_0^{g(x)} f(t) dt$, then $F'(x) = f(g(x)) \cdot g'(x)$

#### Extra

$$
\begin{aligned}
F(x) &= \int_\pi^{\ln(3x + s)} \frac{t^2 + \sin t}{\sqrt{3t - s}} dt \\
F'(x) &= \frac{\ln (3x+5)^2 + \sin (\ln 3x + 5)}{\sqrt{3 (\ln 3x+5) - 5}} \cdot \frac1{3x + 5} \cdot 3
\end{aligned}
$$