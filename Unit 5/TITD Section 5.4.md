# TITD Section 5.4

1.

$$
\begin{aligned}
\text{Let } u = \sqrt{x} \\
\implies& \frac{d}{dx} \int_1^u \frac{t^2}{t^2 + 1} dt \\
= \ & \frac{d}{du} \left(\int_1^u \frac{t^2}{t^2 + 1} \ dt\right) \frac{du}{dx} \\
= \ & \frac{u^2}{u^2 + 1} \frac{du}{dx} \\
= \ & \frac{x}{x+1} \cdot \frac1{2\sqrt{x}} \\
\end{aligned}
$$

2.

$$
\begin{aligned}
    & \frac{d}{dx} \int_3^x e^{t^2-t} \ dt \\
= \ & e^{x^2 - x} (2x - 1)
\end{aligned}
$$

3.

$$
\begin{aligned}
\text{Let } u = \tan x \\
\implies& \frac{d}{dx} \int_0^u \sqrt{t + \sqrt{t}} \ dt \\
= \ & \frac{d}{du} \left(\int_0^u \sqrt{t + \sqrt{t}} \ dt \right) \frac{du}{dx} \\
= \ & \sqrt{u + \sqrt{u}} \cdot \frac{du}{dx} \\
= \ & \sqrt{\tan x + \sqrt{\tan x}} \cdot \sec^2 x
\end{aligned}
$$