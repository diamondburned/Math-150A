# TITD Section 5.2 - 5.3

$$
\begin{aligned}
\int_a^b f(x) dx = [F(x)]_a^{\ \ b} = F(b) - F(a)
\end{aligned}
$$

1.

$$
\begin{aligned}
\int_1^2 & 4x^3 - 3x^2 + 2x \ dx = \int_1^2 x^4 - x^3 + x^2
\end{aligned}
$$

$$
\begin{aligned}
F(1) &= 1^4 - 1^3 + 1^2 = 1 \\
F(2) &= 2^4 - 2^3 + 2^2 = 12 \\
\end{aligned}
$$

$$
\begin{aligned}
\int_1^2 = F(2) - F(1) = 12-1 = 11
\end{aligned}
$$

2.

$$
\begin{aligned}
\int_{\frac\pi4}^{\frac\pi3} \csc^2 x \ dx = \int -\cot x
\end{aligned}
$$

$$
\begin{aligned}
F(\frac\pi4) &= -\cot \frac\pi4 = 1 \\
F(\frac\pi3) &= -\cot \frac\pi3 = \frac{\sqrt3}3 \\
\end{aligned}
$$

$$
\begin{aligned}
\int_{\frac\pi4}^{\frac\pi3} = \frac{\sqrt3}3 - 1 = -\frac{3+\sqrt3}{3}
\end{aligned}
$$