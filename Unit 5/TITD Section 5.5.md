# TITD Section 5.5

1.

$$
\begin{aligned}
&\int x^3 (2+x^4)^5 dx \text{ let } u = 2 + x^4 \\
\frac{du}{dx} &= 4x^3 \\
dx &= \frac{du}{4x^3} \\
&\int x^3 u^5 \frac{du}{4x^3} \\
&= \frac14 \int u^5 du \\
&= \frac14 \cdot (\frac16 u^6 + C) \\
&= \frac{(2+x^4)^6}{24} + \frac{C}4
\end{aligned}
$$

2.

$$
\begin{aligned}
&\int \sec^3 x \tan x dx \text{ let } u = \sec x \\
\frac{du}{dx} &= \sec x \tan x \\
dx &= \frac{du}{\sec x \tan x} \\
&\int \sec^3 x \tan x \frac{du}{\sec x \tan x} \\
&= \int \sec^2 x du \\
&= \int u^2 du \\
&= \frac13 u^3 + C \\
&= \frac13 \sec^3 x + C
\end{aligned}
$$

3.

$$
\begin{aligned}
&\int_0^1 (3t - 1)^{50} dt \text{ let } u = 3t - 1 \\
\frac{du}{dt} &= 3 \\
dt &= \frac{du}3 \\
&\int u^{50} \frac13 du \\
&= \frac13 \int u^{50} du \\
&= \frac13 \cdot \frac1{51} u^{51} \\
&= \frac{u^{51}}{153} \\
&= \frac{(3t - 1)^{51}}{153} \\
&\left[\frac{(3t - 1)^{51}}{153}\right]_0^1 \\
&= \frac{2^{51} + 1}{153}
\end{aligned}
$$

4.

$$
\begin{aligned}
&\int_1^2 x \sqrt{x-1} dx \text{ let } u = x - 1 \\
\frac{du}{dx} &= 1 \\
dx &= du \\
&= \int_1^2 x dx \cdot \int_1^2 \sqrt{u} du \\
&= \left[\frac12x^2\right]_1^2 \cdot \left[\frac23 u^{\frac32}\right]_1^2 \\
&= \left[\frac12x^2\right]_1^2 \cdot \left[\frac23 (x-1)^{\frac32}\right]_1^2 \\
&= \frac32 \cdot \frac23 = 1
\end{aligned}
$$