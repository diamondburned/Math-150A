# Classwork Section 5.5

### Ex #1

A.

$$
\begin{aligned}
\text{Let } u &= x^2 + 1 \\
du &= 2x dx \\
dx &= \frac{du}{2x} \\
&\int u^2 \cdot 2x \cdot \frac{du}{2x} \\
&= \int u^2 \cdot du \\
&= \frac{u^3}{3} + C \\
&= \frac{(x^2 + 1)}3 + C
\end{aligned}
$$

B.

$$
\begin{aligned}
\text{Let } u &= 4x \\
du &= 4dx \\
dx &= \frac{du}4 \\
&\int \cos u du \\
&= \sin u + C \\
&= \sin 4x + C
\end{aligned}
$$

C.

$$
\begin{aligned}
\text{Let } u &= 4x^2 + 1 \\
du &= 8x dx \\
dx &= \frac{du}{8x} \\
&\int 8x \sqrt{4x^2 + 1} dx \\
&= \int 8x \sqrt{u} \frac{du}{8x} \\
&= \int \sqrt{u} du \\
&= \int u^{\frac12} du \\
&= \frac23 u^{\frac32} + C \\
&= \frac23 (4x^2)^{\frac32} + C
\end{aligned}
$$

D.

$$
\begin{aligned}
\text{Let } u &= x^2 + 2 \\
du &= 2x dx \\
dx &= \frac{du}{2x} \\
&\int xu^3 \frac{du}{2x} \\
&= \frac12 \int u^3 du \\
&= \frac12 \cdot \frac14 u^4 \\
&= \frac18 u^4 + C \\
&= \frac18 (x^2 + 2)^4 + C
\end{aligned}
$$

E.

$$
\begin{aligned}
\text{Let } u &= 4x^2 + 3 \\
du &= 8x dx \\
dx &= \frac{du}{8x} \\
&\int \frac{x}{u^6} \cdot \frac{du}{8x} \\
&= \frac18 \int u^{-6} dx \\
&= \frac18 \left[\frac{u^{-5}}{-5}\right] + C \\
&=-\frac1{40(4x^2+3)^5} + C
\end{aligned}
$$

F.

$$
\begin{aligned}
\text{Let } u &= 7 - 2x^3 \\
du &= -6x^2 dx \\
dx &= \frac{du}{-6x^2} \\
&\int u^{\frac43} x^2 \cdot \frac{du}{-6x^2} \\
&= -\frac16 \int u^{\frac43} du \\
&= -\frac16 \left[\frac37 u^{\frac73}\right] + C \\
&= -\frac1{14} \sqrt[3]{(7-2x^3)^7} + C
\end{aligned}
$$

G.

$$
\begin{aligned}
\text{Let } u &= \sin x + 2 \\
du &= \cos x dx \\
dx &= \frac{du}{\cos x} \\
&\int \cos x \sqrt{u} \frac{du}{\cos x} \\
&= \int \sqrt{u} du \\
&= \frac23 u^{\frac32} \\
&= \frac23 (x+2)^{\frac32}
\end{aligned}
$$

H.

$$
\begin{aligned}
\text{Let } u &= 4t^3 \\
dt &= \frac{du}{12t^2} \\
& \int 12t^2 e^4 \frac{du}{12t^2} \\
&= \int e^u du \\
&= e^u + C \\
&= e^{4t^3} + C
\end{aligned}
$$

I.

$$
\begin{aligned}
\text{Let } u = \ln x \\
dx &= xdu \\
\int \frac{u}x \cdot x du \\
&= \int udu \\
&= \frac{u^2}2 + C \\
&= \frac12 \left[\ln x\right]^2 + C
\end{aligned}
$$

J.

$$
\begin{aligned}
\text{Let } u &= \cos x \\
dx &= \frac{du}{-\sin x} \\
&\int u^4 \sin x \cdot \frac{du}{-\sin x} \\
&= -\int u^4 du \\
&= - \frac{u^5}5 + C \\
&= -\frac{\cos^5 x}5 + C
\end{aligned}
$$

K.

$$
\begin{aligned}
&\int (3x^2 + 1)(e^{x^3 + x}) dx \\
\text{Let } u &= x^3 + x \\
du &= 3x^2 + 1 dx \\
dx &= \frac{du}{3x^2 + 1} \\
& \int (3x^2+1)\cdot e^u \cdot \frac{du}{3x^2 + 1} \\
&= \int e^u du \\
&= e^u + C \\
&= e^{x^3 + x} + C
\end{aligned}
$$

L.

$$
\begin{aligned}
\text{Let } u &= 3 + e^{3t} \\
du &= 3e^{3t} dt \\
dt &= \frac{du}{3e^{3t}} \\
&\int \frac{e^{3t}}u \cdot \frac{du}{3e^{3t}} \\
&= \frac13 \ln |u| + C \\
&= \frac13 \ln \left|3+e^{3t}\right| + C
\end{aligned}
$$

M.

$$
\begin{aligned}
&= \int 3^x dx + \int \frac1x dx \\
&= \frac{3^x}{\ln 3} + \ln |x| + C \\
\end{aligned}
$$

N.

$$
\begin{aligned}
& \int (x \cos x^2)(\sin x^2 + 5) dx \\
\text{Let } u &= \sin x^2 + 5 \\
du &= \cos x^2 \cdot 2x \\
dx &= \frac{du}{\cos x^2 \cdot 2x} \\
& \int x \cos x^2 \cdot u \cdot \frac{du}{\cos x^2 \cdot 2x} \\
&= \frac12 \int u du \\
&= \frac12 \cdot \frac{u^2}2 + C \\
&= \frac{u^2}4 + C \\
&= \frac{(\sin x^2 + 5)^2}4 + C
\end{aligned}
$$

### Ex #2

A.

$$
\begin{aligned}
& \int_0^{\frac\pi4} \tan x \cdot \sec^2 x dx \\
\text{Let } u &= \tan x \\
du &= \sec^2 x dx \\
dx &= \frac{du}{\sec^2 x} \\
& \int_0^{\frac\pi4} u \cdot \sec^2 x \frac{du}{\sec^2 x} \\
&= \int_0^{\frac\pi4} u du \\
&= \left[\frac{u^2}2\right]_0^{\frac\pi4} \\
&= \left[\frac{(\tan x)^2}{2}\right]_0^{\frac\pi4} \\
&= \frac12
\end{aligned}
$$

B.

$$
\begin{aligned}
&\int_0^\frac\pi6 \frac{\sin 2\theta}{\cos^3 2 \theta} \\
\text{Let } u &= \cos 2\theta \\
du &= 2 \sin 2 \theta d\theta \\
d\theta &= \frac{du}{2 \sin 2\theta} \\
&\int_0^\frac\pi6 \frac{\sin 2\theta}{u^3} \cdot \frac{du}{2 \sin 2 \theta} \\
&= -\frac12 \int_0^\frac\pi6 \frac1{u^3} du \\
&= -\frac12 \int_0^\frac\pi6 u^{-3} du \\
&= -\frac12 \int_0^\frac\pi6 \frac{1}{-2u^2} du \\
&= \frac14 \left[\frac1{\cos^2 2\theta}\right]_0^\frac\pi6 \\
&= \frac34
\end{aligned}
$$

C.

$$
\begin{aligned}
\text{Let } u &= 1 + x^3 \\
du &= 3x^2 dx \\
dx &= \frac{du}{3x^2} \\
x &= 0 \therefore u = 1 \\
x &= 2 \therefore u = 9 \\
&\int_1^9 3x^2 \cdot \sqrt{u} \frac{du}{3x^2} \\
&= \int_1^9 u^\frac12 du \\
&= \left[\frac23 u^{\frac32}\right]_1^9 \\
&= \frac{52}3
\end{aligned}
$$

D.

$$
\begin{aligned}
\text{Let } u &= 1 + 5x^2 \\
du &= 10x dx \\
dx &= \frac{du}{10x} \\
& \int_1^6 \frac{x}u \cdot \frac{du}{10x} \\
&= \frac1{10} \int_1^6 \frac1u du \\
&= \frac1{10} \left|ln u\right|_1^6 \\
&= \frac1{10} [\ln 6 - \ln 1] \\
&= \frac{\ln 6}{10}
\end{aligned}
$$