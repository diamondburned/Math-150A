# TITD Section 5.1

## Conceptual

![](2020-11-03-20-39-20.png)

## Example

![](2020-11-03-20-47-41.png)

$$
\begin{aligned}
 \Delta x &= (3-1)/4 = 0.5 \\
        A &= x \cdot (x^2 + 1) \\
A_{total} &= (1 \cdot (1^2 + 1)) + (1.5 \cdot (1.5^2 + 1)) + (2 \cdot (2^2 + 1)) + (2.5 \cdot (2.5^2+1))
          &= 35
\end{aligned}
$$

For this function, we have underestimated the area under the curve. This is because the left-hand Riemann sum equation does not account for the small area above the rectangle.