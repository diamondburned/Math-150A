### 2.7 Related Rates

Know, given, find

### 2.8 Linear Approximation

Approx $y = m(x-x_p) + y$

$$
\begin{aligned}
y = 2xy + 3x^2 \\
\end{aligned}
$$

### 3.7 L'Hopital's Rule

$$
\begin{aligned}
\frac00, \frac\infty\infty \\
\limfn{x}{a} \frac00
\end{aligned}
$$

### 4.1 Max + Min values

EVT - continuous over $[a, b]$

Check both endpoints and critical values (derivative = 0).

### 4.2 Mean Value + Rolle's Theorem

MVT: *state* continuous and differentiable function

$$
\begin{aligned}
f'(c) &= \frac{f(b) - f(a)}{b-a}
\end{aligned}
$$

Rolle's Theorem: *state* continuous and differentiable function

$$
\begin{aligned}
f(a) &= f(b); f'(c) = 0
\end{aligned}
$$

### 4.3 Behavior of the function

Incerasing/decreasing; concave up/down

### 4.4 Curve Sketchingo

### Optimization Problems

$$
\begin{aligned}
f'(c) = 0; f''(c) > 0 => \text{local min} \\
f'(c) = 0; f''(c) < 0 => \text{local max}
\end{aligned}
$$