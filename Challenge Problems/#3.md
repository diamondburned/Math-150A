# Challenge Problem #3

### Part 1

$$
\begin{aligned}
&\frac{d}{dx}\left[f(x) + g(x)\right] = f'(x) + g'(x) \\
&\frac{d}{dx}\left[f(x) - g(x)\right] = f'(x) - g'(x) \\
&\frac{d}{dx}\left[f(x) \cdot g(x)\right] = f'(x) \cdot g(x) + f(x) \cdot g'(x) \\
&\frac{d}{dx}\left[\frac{f(x)}{g(x)}\right] = \frac{\left[f'(x) \cdot g(x)\right]-\left[f(x) \cdot g'(x)\right]}{g(x)^2} \\
&\frac{d}{dx}\left[f(g(x))\right] = f'(g(x)) \cdot g'(x)
\end{aligned}
$$

### Part 2

1.

$$
\text{If } h(x) = [f(x) + g(x)] \text{, then } h'(2) = -\frac12
$$

2. 

$$
\text{If } m(x) = [f(x) + g(x)] \text{, then } m'(2) = \frac52
$$


3. 
   
$$
\begin{aligned}
\text{If } n(x) &= \left[\frac{f(x)}{g(x)}\right] \text{, then} \\
n'(x) &= f'(x) \cdot g(x) + f(x) \cdot g'(x) \\
n'(4) &= 1
\end{aligned}
$$

4. 

$$
\begin{aligned}
\text{If } p(x) &= \left[\frac{f(x)}{g(x)}\right] \text{, then} \\
p'(x) &= \frac{\left[f'(x) \cdot g(x)\right]-\left[f(x) \cdot g'(x)\right]}{g(x)^2} \\
p'(1) &= \frac12
\end{aligned}
$$

5. 

$$
\begin{aligned}
\text{If } w(x) &= [f(g(x))] \text{, then} \\
w'(x) &= f'(g(x)) \cdot g'(x) \\
w'(3) &= f'(1) \cdot 0 \\
w'(3) &= 0
\end{aligned}
$$