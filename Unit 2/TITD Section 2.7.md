# TITD Section 2.7

### Guidelines to solving related rates problems

1. Make a _sketch_ and _label_ the quantities.
2. Read the problem and identify all quantities as: _Know_, _Given_ and _Find_
   with the appropriate information.
3. Write an _equation_ involving the variables whose _rates_ of _change_ either
   given or are to be determined.
4. Using the _Chain Rule_, implicitly differentiate both sides of the equation
   with respect to time, __t__.
5. _Substitute_ into the resulting equation all known values for the variables
   and their rates of change. Then _solve_ for the required rate of change.

### The Spreading Circle

#### Know

$$
\begin{aligned}
A = \pi r^2
\end{aligned}
$$

#### Given

$$
\begin{aligned}
\frac{Dr}{dt} = 30 \text{m/hr}
\end{aligned}
$$

#### Find

$$
\begin{aligned}
\frac{DA}{dt} \Bigr|_{r = 100 \text{m}}
\end{aligned}
$$