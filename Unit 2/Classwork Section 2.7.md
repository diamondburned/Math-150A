# Classwork Section 2.7

### Together

a.

$$
\begin{aligned}
\frac{dy}{dt} &= 2x \frac{dx}{dt} \\
&= 2(1)(4) = 8
\end{aligned}
$$

b.

$$
\begin{aligned}
\frac{dy}{dt} &= 4x \frac{dx}{dt} \\
-1 &= 4(-1) \frac{dx}{dt} \\
\frac{dx}{dt} &= \frac14
\end{aligned}
$$

c.

$$
\begin{aligned}
z^2 &= x^2 + y^2 \\
z^2 &= 1^2 + 3^2 \\
z &= \sqrt{10}
\\[1em]
2z \frac{dz}{dt} &= 2x \frac{dx}{dt} + 2y \frac{dy}{dt} \\
2 \sqrt{10} \frac{dz}{dt} &= 2(1)(4) + 2(3)(3) \\
2 \sqrt{10} \frac{dz}{dt} &= 8 + 18 = 26 \\
\frac{dz}{dt} &= \frac{13}{\sqrt{10}}
\end{aligned}
$$

### Together

a.

$$
\begin{aligned}
\frac{dV}{dt} &= 6 \ \text{ft}^3\text{/s}
\end{aligned}
$$

b.

$$
\begin{aligned}
\frac{dh}{dt} &= -2 \ \text{in/h}
\end{aligned}
$$

c.

$$
\begin{aligned}
\frac{dA}{dt} &= 85 \ \text{vehicles/h}
\end{aligned}
$$

d.

$$
\begin{aligned}
\frac{dV}{dt} &= -15 \ \text{cm}^3\text{/s}
\end{aligned}
$$

### Groups

a.

$$
\begin{aligned}
\frac{dA}{dt} &= \frac{dl}{dt} \cdot \frac{dw}{dt}
\end{aligned}
$$

b.

$$
\begin{aligned}
\frac{dC}{dt} &= 2 \pi \frac{dr}{dt}
\end{aligned}
$$

c.

$$
\begin{aligned}
\frac{dV}{dt} &= 2s \frac{ds}{dt}
\end{aligned}
$$

d.

$$
\begin{aligned}
\frac{dS}{dt} &= 2 \pi \frac{dr}{dt} h + 2 \pi r \frac{dh}{dt}
\end{aligned}
$$

e.

$$
\begin{aligned}
\frac{dA}{dt} &= 2 \pi r \frac{dr}{dt}
\end{aligned}
$$

f.

$$
\begin{aligned}
\frac{dP}{dt} &= 4 \frac{ds}{dt}
\end{aligned}
$$

g.

$$
\begin{aligned}
\frac{dV}{dt} &= 2 \pi r \frac{dr}{dt} h + \pi r^2 \frac{dh}{dt}
\end{aligned}
$$

h.

$$
\begin{aligned}
\frac{dV}{dt} &= \frac23 \pi r \frac{dr}{dt} + \frac13 \pi r^2 \frac{dh}{dt}
\end{aligned}
$$

i.

$$
\begin{aligned}
\frac{dV}{dt} &= 4 \pi r^2 \frac{dr}{dt}
\end{aligned}
$$

j.

$$
\begin{aligned}
2x \frac{dx}{dt} + 2y \frac{dy}{dt} &= 0
\end{aligned}
$$

### Together

1. Know

$$
\begin{aligned}
A = 2 \pi r
\end{aligned}
$$

2. Given

$$
\begin{aligned}
\frac{dr}{dt} &= 1 \ \text{ft=/s}
\end{aligned}
$$

3. Find

$$
\begin{aligned}
\frac{dA}{dt} \Bigl|_{r = 4 \ \text{ft}}
\end{aligned}
$$

$$
\begin{aligned}
\frac{dA}{dt} &= 2 \pi r \frac{dr}{dt} \\
&= 2 \pi (4) (1) &= 8 \pi \ \text{ft}^2\text{/s}
\end{aligned}
$$

### Groups

1. Know

$$
\begin{aligned}
V &= \frac43 \pi r^3
\end{aligned}
$$

2. Given

$$
\begin{aligned}
\frac{dV}{dt} &= 800 \ \text{cm}^3\text{/min}
\end{aligned}
$$

3. Find

$$
\begin{aligned}
\frac{dr}{dt} \Bigl|_{r = 30 \ \text{cm}}
\end{aligned}
$$

$$
\begin{aligned}
\frac{dV}{dt} &= 4 \pi r^2 \frac{dr}{dt} \\
800 &= 4 \pi (30)^2 \frac{dr}{dt} \\
\frac{dr}{dt} &= \frac{800}{4\pi (30)^2} \\
\frac{dr}{dt} &= \frac{200}{30^2 \pi} \\
\frac{dr}{dt} &= \frac{2}{9\pi} \ \text{cm/min} \\
\end{aligned}
$$

### Groups

Assume the triangle:

```
  |\
  | \
y |  \ 25
  |   \
  |____\
     x
```

1. Know

$$
\begin{aligned}
x^2 + y^2 = c^2
\end{aligned}
$$

2. Given

$$
\begin{aligned}
x^2 + y^2 &= 25 \\
\frac{dy}{dt} &= -3 \ \text{ft/min}
\end{aligned}
$$

3. Find

$$
\begin{aligned}
\frac{dx}{dt} \Bigl|_{y=7}
\end{aligned}
$$

$$
\begin{aligned}
7^2 + x^2 &= 25^2 \\
a &= 24
\\[1em]
2x \frac{dx}{dt} + 2y \frac{dy}{dt} &= 0 \\
2 (24) \frac{dx}{dt} + 2 (7) (-3) &= 0 \\
48 \frac{dx}{dt} &= 42 \\
\frac{dx}{dt} &= \frac{42}{48} &= \frac{7}{8} \text{ft/min}
\end{aligned}
$$

### Groups

1. Know

$$
\begin{aligned}
A = \pi r^2
\end{aligned}
$$

2. Given

$$
\begin{aligned}
\frac{dA}{dt} &= 10 \ \text{mm}^2\text{/s}
\end{aligned}
$$

3. Know

$$
\begin{aligned}
\frac{dr}{dt} \Bigl|_{r = 116 \ \text{mm}}
\\
\end{aligned}
$$

$$
\begin{aligned}
\frac{dA}{dt} &= 2 \pi r \frac{dr}{dt} \\
10 &= 2 \pi (116) \frac{dr}{dt} \\
\frac{dr}{dt} &= \frac{10}{232\pi} \\
\frac{dr}{dt} &= \frac{5}{116\pi} \text{mm/sec}
\end{aligned}
$$

### Groups

1. Know

$$
\begin{aligned}
V = s^3
\end{aligned}
$$

2. Given

$$
\begin{aligned}
\frac{ds}{dt} &= 3 \ \text{cm/s}
\end{aligned}
$$

3. Find

$$
\begin{aligned}
\frac{dV}{dt} \Bigl|_{s = 10 \ \text{cm}}
\\
\end{aligned}
$$

$$
\begin{aligned}
\frac{dV}{dt} &= 3s^2 \frac{ds}{dt} \\
&= 3 (10)^2 (3) &= 900 \ \text{cm}^3\text{/s}
\end{aligned}
$$

### Together

????

### Groups

1. Know

$$
\begin{aligned}
V = \pi r^2 h
\end{aligned}
$$

2. Given

$$
\begin{aligned}
\frac{dr}{dt} &= -2 \ \text{in/s} \\
\frac{dh}{dt} &= 5 \ \text{in/s}
\end{aligned}
$$

3. Find

$$
\begin{aligned}
\frac{dV}{dt} \Bigl|_{\substack{r = 4 \ \text{in} \\ h = 6 \ \text{in}}}
\end{aligned}
$$

$$
\begin{aligned}
\frac{dV}{dt} &= 2 \pi r \frac{dr}{dt} \cdot h + \pi r^2 \frac{dh}{dt} \\
&= 2\pi (4) (2) (6) + 4^2 \pi (5) \\
&= -96\pi + 80\pi \\
&= -16\pi \ \text{in}^3\text{/s}
\end{aligned}
$$

### Groups
