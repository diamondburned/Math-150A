# TITD Section 2.6

### Conceptual

Example of a function in an explicit form:

$$
\begin{aligned}
y = x^3 - 2x^2 + x - 3
\end{aligned}
$$

Example of a function in an implicit form:

$$
\begin{aligned}
x^3 - 2x^2 + x - y = 3
\end{aligned}
$$

$f'(x)$ can also be written as $\frac{d}{dx}x$

### Example

1.

$$
\begin{aligned}
&\frac{d}{dx}(x^2) = 2x \\
&\frac{d}{dx}(y^2) = 2y \frac{dy}{dx} \\
&\frac{d}{dx}(1) = 0
\end{aligned}
$$

2.

$$
\begin{aligned}
\frac{d}{dx}(x^2 + y^2) &= 0 \\
2x + 2y \frac{dy}{dx} &= 0
\end{aligned}
$$

3.
   
$$
\begin{aligned}
2y \frac{dy}{dx} &= -2x \\
\frac{dy}{dx} &= -\frac{x}{y}
\end{aligned}
$$

4. Equation for the circle: $x^2 + y^2 = 1$

$$
\begin{aligned}
\frac{d}{dx}(x^2 + y^2) &= 0 \\
2x + 2y \frac{dy}{dx} &= 0 \\
\frac{dy}{dx} &= -\frac{x}{y} \\
m &= \frac{dy}{dx}(\frac12, \frac{\sqrt3}2) = -\frac{\frac12}{\frac{\sqrt3}{2}} = -\frac{\sqrt3}{3}
\end{aligned}
$$

![](2020-09-22-22-39-41.png)