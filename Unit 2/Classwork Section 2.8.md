# Classwork Section 2.8

### Together

a.

$$
\begin{aligned}
m &= \frac1{2\sqrt{x}} = \frac18 \\[1em]
y-4 &= \frac18(x-16) \\
y &= \frac18(x-16)+4 \\[1em]
y &= \frac18(15.8-16) + 4 \\
y &= \frac18(-\frac2{10}) + 4 \\
y &= -\frac{1}{40} + 40 \\
y &= -0.025 + 4 = 3.975 \\
\end{aligned}
$$

b.

$$
\begin{aligned}
y &= x^4 \ (3, 81) \\[1em]
m &= 4x^3 \\
m &= 4(3)^3 \\
m &= 108 \\[1em]
y - 80 &= 108 (x-3) \\
y &= 108(x-3) + 81 \\[1em]
y &= 108(2.99-3) + 81 \\
y &= 108(-0.01) + 81 \\
y &= -1.08 + 81 \\
y &= 79.92
\end{aligned}
$$

c.

$$
\begin{aligned}
y &= \sin x \ (90, 1) \\[1em]
m &= \cos x \\
m &= 0 \\[1em]
y - 1 &= 0 (x-90) \\
y &= 1
\end{aligned}
$$

d.

$$
\begin{aligned}
y &= \sqrt{x} \ (25, 5) \\[1em]
m &= \frac12x^{-\frac12} \\
m &= \frac1{2 \sqrt{x}} \\
m &= \frac1{2\sqrt{25}} \\
m &= \frac1{10} \\[1em]
y - 5 &= \frac1{10}(x - 25) \\
y &= \frac1{10}x - \frac{5}{2} + 5 \\
y &= \frac1{10}x + \frac{5}{2} \\[1em]
y &= \frac{24.8}{10} + \frac52 \\
y &= \frac{49.8}{10} \\[.8em]
y &= 4.98
\end{aligned}
$$

### Groups

e.

$$
\begin{aligned}
y &= \sqrt[3]{x} \ (8, 2) \\[1em]
m &= \frac13(x)^{-\frac{2}{3}} \\
&= \frac1{3 \sqrt[3]{x}^2} \\[1em]
m &= \frac1{3 \sqrt[3]{8}^2} \\
&= \frac1{12} \\[1em]
y - 2 &= \frac1{12}(x - 8) \\
y &= \frac{x}{12} - \frac23 + 2 \\
y &= \frac{x}{12} + \frac43 \\[1em]
y &= \frac{7.7}{12} + \frac43 \\
y &= \frac{77}{120} + \frac{160}{120} \\
y &= \frac{237}{120} \\
y &= 1.975
\end{aligned}
$$

f.

$$
\begin{aligned}
y &= \cos x \ (90, 0) \\[1em]
m &= -\sin x \\
m &= -\sin 90 \\
&= -1 \\[1em]
y &= -(x - 90) \\
y &= -x + 90 \\[1em]
y &= -92 + 90 \\
y &= -2
\end{aligned}
$$

g.

$$
\begin{aligned}
y &= x^4 \ (6, 1296) \\[1em]
m &= 4x^3 \\
m &= 4(6)^3 \\
&= 864 \\[1em]
y - 1296 &= 864(x - 6) \\
y &= 864x - 5184 + 1296 \\
y &= 864x - 3888 \\[1em]
y &= 864(6.04) - 3888 \\
y &= 5218.56 - 3888 \\
y &= 1330.56
\end{aligned}
$$

h.

$$
\begin{aligned}
y &= \sqrt{x} \ (9, 3) \\[1em]
m &= \frac12 x^{-\frac12} \\
m &= \frac12(9)^{-\frac12} \\
m &= \frac16 \\[1em]
y - 3 &= \frac16(x-9) \\
y &= \frac16x - \frac32 + 3 \\
y &= \frac16x + \frac32 \\[1em]
y &= \frac16(8.9) + \frac32 \\
y &= \frac{89}{60} + \frac{90}{60} \\
y &= \frac{179}{60} \\
y & \approx 2.983
\end{aligned}
$$