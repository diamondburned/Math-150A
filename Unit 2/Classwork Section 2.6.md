# Classword Section 2.6

### Together

a. 

$$
\begin{aligned}
\frac{d}{dx}(x^3 + y^3) &= \frac{d}{dx}(6xy) \\
3x^2 + 3y^2 \frac{dy}{dx} &= 6y + 6x\frac{dy}{dx} \\
-6 \frac{dy}{dx} + 3y^2 \frac{dy}{dx} &= 6y - 3x^2 \\
\frac{dy}{dx}(6 - 3y^2) &= 6y - 3x^2 \\
\frac{dy}{dx} &= \frac{6y - 3x^2}{6 - 3y^2}
\end{aligned}
$$

b. 

$$
\begin{aligned}
\frac{d}{dx}(\sin(x+y)) &= \frac{d}{dx}(y^2 \cos x) \\
\cos(x+y) (1+y') &= 2yy' \cos x + y^2 (-\sin x) \\
\cos(x+y) + y' \cos (x+y) &= 2yy' \cos x + y^2 (-\sin x) \\
\cos(x+y) + y^2 \sin(x) &= 2yy' \cos x - y' cos(x+y) \\
\cos(x+y) + y^2 \sin(x) &= y' (2y \cos x - cos(x+y)) \\
y' &= \frac{cos(x+y) + y^2 \sin x}{2y \cos x - cos(x+y)}
\end{aligned}
$$

### Together

a. 
  
$$
\begin{aligned}
\frac{d}{dx}(x^3 + y^3) &= \frac{d}{dx}(6xy) \\
3x^2 + 3y^2 \frac{dy}{dx} &= 6y + 6x\frac{dy}{dx} \\
-6x \frac{dy}{dx} + 3y^2 \frac{dy}{dx} &= 6y - 3x^2 \\
\frac{dy}{dx}(-6x + 3y^2) &= 6y - 3x^2 \\
\frac{dy}{dx} &= \frac{6y - 3x^2}{- 6x + 3y^2} \\
\frac{6(3) - 3(3)^2}{- 6(3) + 3(3)^2} &= -1 \\
y-3 &= -1(x-3) \\
y &= -x + 6
\end{aligned}
$$

### Groups

a.

$$
\begin{aligned}
\frac{d}{dx} = 2x + x \frac{dy}{dx} + y + 2y \frac{dy}{dx} &= 0 \\
x \frac{dy}{dx} + 2y \frac{dy}{dx} &= -2x - y \\
m = \frac{dy}{dx} &= -\frac{2x + y}{x + 2y} \\
m(1, 1) &= -\frac{2(1) + 1}{1 + 2(1)} \\
m(1, 1) &= -1
\\[1em] 
y - 1 &= -1(x - 1) \\
y &= -x + 2
\end{aligned}
$$

b.

$$
\begin{aligned}
2x + 2y + 2x \frac{dy}{dx} - 2y \frac{dy}{dx} + 1 &= 0 \\
m = \frac{dy}{dx} &= -\frac{1 + 2x + 2y}{2x - 2y} \\
m(1, 2) &= -\frac{1 + 2(1) + 2(2)}{2(1) - 2(2)} \\
m(1, 2) &= \frac{7}{2}
\\[1em]
y - 2 &= \frac72 (x - 1) \\
y &= \frac72x - \frac32
\end{aligned}
$$