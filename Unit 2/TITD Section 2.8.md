# TITD Section 2.8

a.

![](IMG_20201005_171401.jpg)

b.

$$
\begin{aligned}
f(x) &= \sqrt{x} \\
f(a) &= \sqrt{4} = 2 \\[2em]

f'(x) &= \frac12 \cdot x^{-\frac12} \\
f'(a) &= \frac12 \cdot {4}^{-\frac12} = \frac14 \\[2em]

L(x) &= f(a) + f'(a)(x-a) \\
&= 2 + \frac14(x - 4) \\
\end{aligned}
$$

c.

$$
\begin{aligned}
L(4.36) &= 2 + \frac14(4.36 - 4) \\
&= 2 + \frac{36}{400} \\
&= \frac{836}{400} = \frac{209}{100} \\[1em]
&= 2.09
\end{aligned}
$$