## Classwork Section 2.5

### Groups

1. $$f(x+4) = e^{x+4}$$

2. $$g(2x) = 8^3 + 2x$$

3. $${e^x}^3 + e^x = e^{3x} + e^x$$

4. $${e^x}^3 + x$$

5. math

### Groups

1. $$y = \ln(x^5 + 2x^{-1})$$

2. $$y = \sqrt{e^x + 1}$$

3. $$y = (5\sqrt{x} + x)^{100}$$

### Together

1.

Outside:

$$
\begin{aligned}
y &= f(u) = sin(u) \\
\frac{dy}{dx} &= cos(u)
\end{aligned}
$$

Inside:

$$
\begin{aligned}
g(x) &= 2x^2 + 4 \\
\frac{dy}{dx} &= 4x
\end{aligned}
$$

Together:

$$
\begin{aligned}
\frac{dy}{dx} &= cos(2x^2 + 4) \cdot 4x \\
\frac{dy}{dx} &= 4xcos(2x^2 + 4)
\end{aligned}
$$

2.

Outside:

$$
\begin{aligned}
y &= f(u) = u^5 \\
\frac{dy}{dx} &= 5u^4
\end{aligned}
$$

Inside

$$
\begin{aligned}
g(x) &= u = 2x+5 \\
\frac{dy}{dx} &= 2
\end{aligned}
$$

$$
\begin{aligned}
\frac{dy}{dx} &= 5(2x+5)^4 \cdot 2 \\
\frac{dy}{dx} &= 10(2x+5)^4
\end{aligned}
$$

### Together

1.

$$
\begin{aligned}
y &= (x^3+2)^5 \\
\frac{dy}{dx} &= 5(x^3+2)^4\cdot 3x^2 \\
\frac{dy}{dx} &= 15x^2(x^3+2)^4 \\
\end{aligned}
$$

2.

$$
\begin{aligned}
y &= (3x^4+1)^4 \\
\frac{dy}{dx} &= 4(3x^4+1)^3 \cdot 12x^3 \\
\frac{dy}{dx} &= 48x^3(3x^4+1)^3
\end{aligned}
$$

### Groups

1.

$$
\begin{aligned}
y &= \sqrt{x^4+1} \\
y &= (x^4+1)^{\frac{1}{2}} \\
y' &= \frac{1}{2}(x^4+1)^{-\frac{1}{2}} \cdot \frac{d}{dx}(x^4+1) \\
y' &= \frac{1}{2}(x^4+1)^{-\frac{1}{2}} \cdot 4x^3 \\
y' &= 2x^3 (x^4+1)^{-\frac{1}{2}}
\end{aligned}
$$

2.

$$
\begin{aligned}
y &= \sqrt[3]{5x^3 + 4} \\
y &= (5x^3 + 4)^{\frac{1}{3}} \\
y' &= \frac{1}{3}(5x^3+4)^{-\frac{2}{3}} \cdot \frac{d}{dx}(5x^3 + 4) \\
y' &= \frac{1}{3}(5x^3+4)^{-\frac{2}{3}} \cdot 15x^2 \\
y' &= 5x^2(5x^3+4)^{-\frac{2}{3}}
\end{aligned}
$$

3.

$$
\begin{aligned}
y &= \frac{-5}{x^2 - 3x + 5} \\
y' &= \frac{0(x^2 - 3x + 5) - (-5)(2x - 3)}{{(x^3 - 3x + 5)}^2} \\
y' &= \frac{0 - (-10x + 15)}{{(x^3 - 3x + 5)}^2} \\
y' &= \frac{10x - 15}{{(x^3 - 3x + 5)}^2}
\end{aligned}
$$

4.

$$
\begin{aligned}
y &= (-5x^3 + 2)(-5x + 1)^{-4} \\
y' &= \frac{d}{dx}(-5x^3 + 2)[{(-5x + 1)}^{-4}] + \frac{d}{dx}[(-5x + 1)^{-4}](-5x^3 + 2) \\
y' &= (-15x^2)[{(-5x + 1)}^{-4}] + [-4(-5x+1)^{-5}](-5x^3 + 2) \\
y' &= (-15x^2){(-5x + 1)}^{-4} + (20x^3-8)(-5x+1)^{-5}
\end{aligned}
$$

### Together

1.

$$
\begin{aligned}
y &= \frac{-x^2+2}{(2x^4 - 3)^2} \\
y' &= \frac{(-2x)(2x^4 - 3)^2 + 4(2x^4-3)^3\frac{d}{dx}(2x^4-3)(-x^2+3)}{(2x^4 - 3)^4} \\
y' &= \frac{(-2x)(2x^4 - 3)^2 + (16x^3)(-x^2+3)(2x^4-3)^3}{(2x^4 - 3)^4} \\
y' &= \frac{(-2x)(2x^4 - 3)^2 + (-16x^5+48x^3)(2x^4-3)^3}{(2x^4 - 3)^4}
\end{aligned}
$$

2.

$$
\begin{aligned}
y &= (\frac{5x^3 + 1}{x+1})^3 \\
y' &= 3(\frac{5x^3 + 1}{x+1})^2 \cdot \frac{d}{dx}(\frac{5x^3 + 1}{x+1}) \\
y' &= 3(\frac{5x^3 + 1}{x+1})^2 \cdot \frac{(15x^2)(x+1) - (5x^3+1)}{(x+1)^2} \\
y' &= 3(\frac{5x^3 + 1}{x+1})^2 \cdot \frac{15x^3+15x^2-5x^3-1}{(x+1)^2} \\
y' &= 3(\frac{5x^3 + 1}{x+1})^2 \cdot \frac{10x^3+15x^2-1}{(x+1)^2} \\
y' &= \frac{30x^3+45x^2-3}{(x+1)^2} (\frac{5x^3 + 1}{x+1})^2 \\
\end{aligned}
$$

3.

$$
\begin{aligned}
y &= \frac{\sqrt[5]{4x^2+3}}{x+5} \\
y &= \frac{(4x^2+3)^{\frac15}}{x+5} \\
y' &= \frac{\frac15(4x^2+3)^{-\frac45} \cdot \ 8x}{(x+5)^2} \\
y' &= \frac{\frac{8x}5(4x^2+3)^{-\frac45}}{(x+5)^2}
\end{aligned}
$$

4.

$$
\begin{aligned}
y &= \sin x^2 = \sin(x^2) \\
y' &= \cos(x^2) \cdot \frac{d}{dx}(x^2) \\
y' &= 2x\cos(x^2) 
\end{aligned}
$$

### Groups

5.

$$
\begin{aligned}
y &= \sin^2x = (\sin x)^2 \\
y' &= 2(\sin x) \cdot \frac{d}{dx}(\sin x) \\
y' &= 2 \sin x \cos x
\end{aligned}
$$

6.

$$
\begin{aligned}
y &= \sin(\cos x) \\
y' &= \cos(\cos x) \cdot \frac{d}{dx}(\cos x) \\
y' &= \cos(\cos x) \cdot - (\sin x)
\end{aligned}
$$

### Together

7.

$$
\begin{aligned}
y &= \cot(3x+2) \\
y' &= -\csc^2(3x+2) \cdot 3 \\
y' &= -3\csc^2(3x+2)
\end{aligned}
$$

8.

$$
\begin{aligned}
y &= \sin(\sin 5x^3) \\
y' &= \cos(\sin 5x^3) \cdot [\cos(5x^3) \cdot 15x] \\
y' &= 15x \cos(\sin 5x^3)\cos(5x^3)
\end{aligned}
$$

9.

$$
\begin{aligned}
y &= \sin 5x^5 \sin 3x^2 \\
y' &= 90x^5 \cos(5x^5) \cos(3x^2)
\end{aligned}
$$

10.

$$
\begin{aligned}
y &= \sec 5x^2 \csc x^5 \\
y' &= (\sec 5x^2 \tan 5x^2)(10x)(\csc x^5) + (\sec 5x^2)(-\csc x^5 \cot x^5)(5x^4) \\
y' &= 10x \sec(5x^2) \tan(5x^2) \csc x^5 - 5x^4 \sec(5x^2) \csc x^5 \cot x^5 \\
\end{aligned}
$$

11.

$$
\begin{aligned}
y &= \frac{\csc 4x^2}{\sin 2x^4} \\
y' &= \frac{(-\csc 4x^2 \cot 4x^2)(8x)(\sin 2x^4) - (\csc 4x^2)(\cos 2x^4)(8x^3)}{(\sin 2x^4)^2} \\
y' &= \frac{-8x \csc 4x^2 \cot 4x^2 \sin 2x^4 - 8x^3 \csc 4x^2 \cos 2x^4}{(\sin 2x^4)^2}
\end{aligned}
$$

12.

$$
\begin{aligned}
y &= \frac{\tan x^2}{\cot x^3} \\
y' &= \frac{(2x \sec^2 x^2)(\cot x^3) - (\tan x^2)(3x^2)(-\csc^2 x^3)}{(\cot x^3)^2} \\
y' &= \frac{2x \sec^2 x^2 \cot x^3 + 3x^2 \tan x^2 \csc^2 x^3}{(\cot x^3)^2}
\end{aligned}
$$

